<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SOFTWORK</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- Font Icon -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    
    <script src="../js/sweetalert2.all.min.js"></script>
    <script src="../js/loadings.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

    <!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.php" class="logo">SOFTWORK</a>
					<nav id="nav">
						<a href="../index.php">HOME</a>
						<a href="./registro.php">Registrarme</a>
					</nav>
				</div>
			</header>
			<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

		<!-- Banner
			<section id="banner" style="padding-top: 50px;">
				<div class="inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 text-center py-4">
                                <b>REGISTRARME</b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                    <div class="row">
                                <div class="col-12 py-4">
                                    Nombre <br>
                                    <input class="form-control" type="text" id="nombre">
                                </div>
                                <div class="col-12 py-4">
                                    Apellidos <br>
                                    <input class="form-control" type="text" id="apellidos">
                                </div>
                                <div class="col-12 py-4">
                                    Descripción Personal <br>
                                    <input class="form-control" type="text" id="descripcion_personal">
                                </div>
                                <div class="col-12 py-4">
                                    Telefono <br>
                                    <input class="form-control" type="text" id="telefono">
                                </div>
                                <div class="col-12 py-4">
                                    Correo <br>
                                    <input class="form-control" type="mail" id="correo">
                                </div>
                                <div class="col-12 py-4">
                                    Contraseña <br>
                                    <input class="form-control" type="text" id="contrasenia">
                                </div>
                                <div class="col-12 py-4">
                                    Confirmar Contraseña <br>
                                    <input class="form-control" type="text" id="confirmar_contrasenia">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-ms-6 py-4 mx-3">
                                <div class="row">
                                    <div class="col-12 py-4">
                                        Pregunta de Seguridad <br>
                                        <select name="" id="pregunta_seguridad">
                                            <option value="1">¿Cual es tu comida favorita?</option>
                                            <option value="2">¿Cual es el nombre de tu primer mascota?</option>
                                            <option value="3">¿Cual es tu ciudad de nacimiento?</option>
                                            <option value="4">¿Cual es tu serie favorita?</option>
                                            <option value="5">¿Cual es tu pelicula favorita?</option>
                                            <option value="6">¿Cual es el auto de tus sueños?</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-12 py-4">
                                        Respuesta <br>
                                        <input class="form-control" type="text" id="respuesta_seguridad">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 py-4">
                                        Nivel de Estudios <br>
                                        <select name="" id="grado_academico">
                                            <option value="1">Secundaria</option>
                                            <option value="2">Preparatoria/Bachillerato</option>
                                            <option value="3">Licenciatura / Profesional</option>
                                            <option value="4">Maestría</option>
                                            <option value="5">Doctorado</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 py-4">
                                        Carrera (Opcional) <br>
                                        <input class="form-control" type="text" id="carrera">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 py-4">
                                        Experiencia <br>
                                        <input class="form-control" type="text" id="experiencia">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 py-4">
                                        Direccion <br>
                                        <input class="form-control" type="text" id="direccion">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 py-4">
                                        Estado<br>
                                        <select name="" id="estado">
                                            <option value="1" selected>Queretaro</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 py-4">
                                        Municipio <br>
                                        <select name="" id="municipio">
                                            <option value="1" selected>Queretaro</option>
                                            <option value="2">Corregidora</option>
                                            <option value="3">San Juan del Rio</option>
                                        </select>
                                    </div>
                                </div>
                                <!-
                                <div class="row">
                                    <div class="col-12 fotoPerfil py-4">
                                        Ingresa tu foto de perfil <br>
                                        <input class="form-control" type="file">
                                    </div>
                                </div>--
                                <div class="row">
                                    <div class="col-12 fotoPerfil py-4 mx-3">
                                        <button id="registrar_usuario">Crear Cuenta</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				</div>
            </section>
-->    
            
        <!-- Banner Registro parte 1-->
            
                <section class="banner" style="padding-top: 0px; padding-bottom:0px;">
                    <div class="inner" style="padding:0px; max-width:100%">
                        <div class="main" style="padding: 80px 0px;">


                            <div id="registro_form1">
                                <div class="container" style="padding:0px;">
                                    <div class="booking-content">
                                        <div class="booking-image">
                                        </div>
                                        <div class="booking-form login-form">
                                            <div id="booking-form">
                                                <div style="border-bottom: solid 2px #8c0303; width:auto;">
                                                    <h2 style="margin: 0px !important; text-align:left; width: auto !important;">REGISTRATE</h2>
                                                </div>
                                                
                                                
                                                <div class="form-group form-input">
                                                    <input type="email"id="correo" value="" required />
                                                    <label class="form-label">Email</label>
                                                </div>
                                                <div class="form-group form-input">
                                                    <input type="password" name="phone" id="contrasenia" required />
                                                    <label class="form-label">Contraseña</label>
                                                </div>
                                                <div class="form-group form-input">
                                                    <input type="password" name="phone" id="confirmar_contrasenia" required />
                                                    <label class="form-label">Confirmar Contraseña</label>
                                                </div>
                                                <br>
                                                <div class="form-group form-input">
                                                    <select name="" id="pregunta_seguridad" required>
                                                        <option value="1">¿Cual es tu comida favorita?</option>
                                                        <option value="2">¿Cual es el nombre de tu primer mascota?</option>
                                                        <option value="3">¿Cual es el nombre de tu mejor amigo</option>
                                                        <option value="4">¿Cual es tu serie favorita?</option>
                                                        <option value="5">¿Cual es tu pelicula favorita?</option>
                                                        <option value="6">¿Cual es el auto de tus sueños?</option>
                                                    </select>
                                                    <label class="form-label" style="top: 1px; font-size: 11px; color: #edb77d;">Pregunta de Seguridad</label>
                                                    
                                                </div>
                                                <div class="form-group form-input">
                                                    <input type="text" id="respuesta_seguridad" required />
                                                    <label class="form-label">Respuesta</label>
                                                </div>
                                                <br>
                                                <div class="group">
                                                    <button class="button" id="registrarme_1">Registrarme</button>
                                                    <a href="#" class="vertify-booking">Ya tengo una cuenta!</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Registro parte 2-->
                            <div id="registro_form2" style="display:none">
                                <div class="container" style="padding:0px;">
                                    <div class="booking-content">
                                        <div class="booking-form login-form" style="width:40%">
                                            <div id="booking-form" >
                                                <div style="border-bottom: solid 2px #8c0303; width:auto;">
                                                    <h2 style="margin: 0px !important; text-align:left; width: auto !important;">INFORMACION PERSONAL</h2>
                                                </div>
                                                <br>
                                                <div class="form-group form-input">
                                                    <input type="text" name="name" id="nombre" required/>
                                                    <label for="name" class="form-label">Nombre</label>
                                                </div>
                                                <div class="form-group form-input">
                                                    <input type="text" name="name" id="apellidos"required/>
                                                    <label class="form-label">Apellidos</label>
                                                </div>
                                                <div class="form-group form-input">
                                                    <input type="date"  id="fecha_nacimiento" required/>
                                                    <label class="form-label">Fecha de nacimiento</label>
                                                </div>
                                                <div class="form-group form-input" >
                                                    <input type="text"  id="telefono" required/>
                                                    <label class="form-label">Telefono</label>
                                                </div>
                                                <div class="form-group form-input" >
                                                    <input type="text"  id="direccion" required/>
                                                    <label class="form-label">Direccion</label>
                                                </div>
                                                <div class="form-group form-input">
                                                    <select name="" id="estado" required>
                                                        <option value="1" selected>Queretaro</option>
                                                        <option value="2">Guanajuato</option>
                                                        <option value="3">CDMX</option>
                                                    </select>
                                                    <label class="form-label" style="top: 1px; font-size: 11px; color: #edb77d;">Estado</label>
                                                </div>
                                                <div class="form-group form-input">
                                                    <select name="" id="municipio" required>
                                                        <option value="1">Santiago de Queretaro</option>
                                                        <option value="2">Cadereyta</option>
                                                        <option value="3" selected>San Juan del Rio</option>
                                                    </select>
                                                    <label class="form-label" style="top: 1px; font-size: 11px; color: #edb77d;">Municipio</label>
                                                </div>
                                                <br>
                                                <div class="group" style="padding-bottom: 60px;">
                                                    <button class="button" id="regresar_registro_2" style="width: 45%; float: left; background-color: #819000">Atras</button>
                                                    <button class="button" id="registrarme_2" style="width: 45%; float: right;">Siguiente</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="booking-image" style="width:60%;">
                                        </div>
                                    </div>
                                </div>

                            </div> 
                            <!-- Registro parte 2-->

                            <!-- Registro parte -->
                            <div id="registro_form3" style="display:none">
                                <div class="container" style="padding:0px;">
                                    <div class="booking-content">
                                        <div class="booking-form login-form" style="width:40%">
                                            <div id="booking-form" >
                                                <div style="border-bottom: solid 2px #8c0303; width:auto;">
                                                    <h2 style="margin: 0px !important; text-align:left; width: auto !important;">INFORMACION PROFESIONAL</h2>
                                                </div>
                                                <br>
                                                <div class="form-group form-input">
                                                    <textarea id="descripcion_personal" cols="30" rows="3" maxlength="150" onkeyup="countChars2(this);" required></textarea>
                                                    <label class="form-label">Breve descripcion personal</label>
                                                </div>
                                                <div class="form-group form-input" style="padding-top: 0px; font-size:10px; text-align: end; color:#cccccc;">
                                                    <label id="contador_descripcion"></label>
                                                </div>
                                                <div class="form-group form-input">
                                                    <select name="" id="grado_academico" required>
                                                        <option value="1">Secundaria</option>
                                                        <option value="2">Preparatoria / Bachillerato</option>
                                                        <option value="3" selected>Universidad / Licenciatura</option>
                                                        <option value="4">Maestria</option>
                                                        <option value="5">Doctorado</option>
                                                    </select>
                                                    <label class="form-label" style="top: 1px; font-size: 11px; color: #edb77d;">Grado Academico</label>
                                                </div>
                                                <div class="form-group form-input">
                                                    <input type="text" id="carrera" required />
                                                    <label class="form-label">Carrera / Especialidad</label>
                                                </div>
                                                <div class="form-group form-input">
                                                    <textarea id="experiencia" cols="30" rows="5" maxlength="500" onkeyup="countChars(this);" required></textarea>
                                                    <label class="form-label">Experiencia Profesional</label>
                                                </div>
                                                <div class="form-group form-input" style="padding-top: 0px; font-size:10px; text-align: end; color:#cccccc;">
                                                    <label id="contador_experiencia"></label>
                                                </div>
                                                <br>
                                                <div class="group" style="padding-bottom: 60px;">
                                                    <button class="button" id="regresar_registro_3" style="width: 45%; float: left; background-color: #819000">Atras</button>
                                                    <button class="button" id="registrarme_3" style="width: 45%; float: right;">Finalizar</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="booking-image" style="width:60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Registro parte 3-->

                        </div>
                    </div>
                </section>    
            
        <!-- Banner -->


            <!--

                                        <div class="form-group">
                                            <div class="select-list">
                                                <select name="time" id="time" required>
                                                    <option value="">Time</option>
                                                    <option value="6pm">6:00 PM</option>
                                                    <option value="7pm">7:00 PM</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="select-list">
                                                <select name="food" id="food" required>
                                                    <option value="">Food</option>
                                                    <option value="seasonalfish">Seasonal steamed fish</option>
                                                    <option value="assortedmushrooms">Assorted mushrooms</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-radio">
                                            <label class="label-radio"> Select Your Dining Space</label>
                                            <div class="radio-item-list">
                                                <span class="radio-item">
                                                    <input type="radio" name="number_people" value="2" id="number_people_2" />
                                                    <label for="number_people_2">2</label>
                                                </span>
                                                <span class="radio-item active">
                                                    <input type="radio" name="number_people" value="4" id="number_people_4" checked="checked" />
                                                    <label for="number_people_4">4</label>
                                                </span>
                                                <span class="radio-item">
                                                    <input type="radio" name="number_people" value="6" id="number_people_6" />
                                                    <label for="number_people_6">6</label>
                                                </span>
                                                <span class="radio-item">
                                                    <input type="radio" name="number_people" value="8" id="number_people_8" />
                                                    <label for="number_people_8">8</label>
                                                </span>
                                                <span class="radio-item">
                                                    <input type="radio" name="number_people" value="10" id="number_people_10" />
                                                    <label for="number_people_10">10</label>
                                                </span>
                                            </div>
                                        </div>

            -->


		<!-- Footer -->
			<section id="footer">
				<div class="inner">
					
					<div class="copyright">
						&copy; Untitled Design: <a href="https://templated.co/">TEMPLATED</a>. Images <a href="https://unsplash.com/">Unsplash</a>
					</div>
				</div>
            </section>
    <script src="../js/funciones_registro.js"></script>
    <script src="../js/peticiones_registro.js"></script>
</body>

</html>