<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SOFTWORK</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- Font Icon -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    
    <script src="../js/sweetalert2.all.min.js"></script>
    <script src="../js/loadings.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
    
<button id="boton">
  Launch demo modal
</button>

<!-- Modal -->
<div id="modal_nomina_operadores" class="modal fade" role="dialog" style="display: none;">
		<div class="modal-dialog modal-lg" style="width: 85% !important;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center">Nomina Operador: <span id="nombre_operador"></span> - Unidad Asignada: <span id="unidad_asignada"></span></h4>
					<h4 class="modal-title text-center" style="display:none" id="nomina_op_pagada"></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8" style="text-align: center; padding:10px; color: white; background-color:#074571; border-radius: 5px; margin-bottom: 10px;">
									<span id="notificacion_nomina_op"></span>
								</div>
							</div>
							<hr style="border-top: 1px solid #cecece !important; width: 95% !important;">

							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<!-- PANEL SERVICIOS -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<button id="btn_panel_servicios" style="float:left;" class="btn_dropdown"  data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											SERVICIOS
										</button>
										<div style="text-align: right; float:right; padding: 0px 15px;">
											<h4 id="rango_fechas"></h4>
										</div>
									</div>

									<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body" style="border: 0px;">
											<input type="hidden" id="id_nomina_op">
											<input type="hidden" id="id_operador">
											<input type="hidden" id="id_unidad">
											<input type="hidden" id="fecha_inicial">
											<input type="hidden" id="fecha_final">
											<table id="table_servicios_operador" class="table table-hover table-bordered">
												<thead style="background-color: #2f3135; color: white;">
													<tr>
														<th>Folio</th>
														<th>Fecha Finalizacion</th>
														<th>Origen</th>
														<th>Destino</th>
														<th>Num Eco</th>
														<th>Capacidad</th>
														<th>Deduccion</th>
														<th>Serv. Adicional</th>
														<th>Costo Servicio</th>
														<th>Costo Final</th>
													</tr>
												</thead>
												<tbody id="body_servicios_operador" style="max-height:50px; overflow-y: scroll;">
												</tbody>
											</table>
											<div class="col-md-12"><br></div>
											<div class="col-md-12">
												<div class="col-md-5" style="text-align: right;">
													<h4>Productividad Minima: $<span id="prod_minima"></span></h4>
												</div>
												<div class="col-md-2">
												</div>
												<div class="col-md-5" style="text-align: left;">
													<h4 style="float:left !important;">Productividad Alcanzada: $ </h4>
													<div id="div_productividad" style="margin-left:5px; text-align:center; margin-top:5px; border-radius: 4px; float:left; height:28px; width:100px; font-size: 18px;"> <span id="costo_servicios"></span></div>
													</h4>
												</div>
											</div>
											<hr>

											<div class="col-md-12">
												<div class="col-md-1"></div>
												<div class="col-md-3" style="padding: 0px;">
													<div class="col-md-5" style="text-align: center; padding: 0px;">
														<h5>Salario Diario: $</h5>
														<input class="form-control move" style="width: 100%;" type="text" id="salario_diario_aut" readonly>
													</div>
													<div class="col-md-2" style="text-align: center;">
														<h4>x</h4>
													</div>
													<div class="col-md-5" style="text-align: center; padding: 0px;">
														<h5>Días Laborados (7):</h5>
														<input type="number" class="form-control move" style="width: 100%;" id="dias_laborados">
													</div>
												</div>


												<div class="col-md-8">
													<div class="col-md-1" style="text-align: right;">
														<h3 style="margin-top:10px;">=</h3>
													</div>
													<div class="col-md-4" style="text-align: right;">
														<h4>Salario Semanal Aplicado:</h4>
													</div>
													<div class="col-md-3">
														<input type="text" class="form-control move" style="width: 100%;" id="salario_semanal_aplicado">
													</div>
													<div class="col-md-3">
														<h5>(Recomendado: $<span>1019.2</span>)</h5>
													</div>
												</div>


												<div class="col-md-4"></div>
												<div class="col-md-8">
													<div class="col-md-5">&nbsp;</div>
													<div class="col-md-3" style="text-align: center;">
														<h3 style="margin:0px;">+</h3>
													</div>
													<div class="col-md-3">
														&nbsp;
													</div>
												</div>

												<div class="col-md-4"></div>
												<div class="col-md-8">
													<div class="col-md-5" style="text-align: right;">
														<h4>Ajuste Salario:</h4>
													</div>
													<div class="col-md-3">
														<input type="text" class="form-control move" style="width: 100%;" id="ajuste_salario_aplicado">
													</div>
													<div class="col-md-3">
														<h5>(Recomendado: $<span>730.8</span>)</h5>
													</div>
												</div>

												<div class="col-md-4"></div>
												<div class="col-md-8">
													<div class="col-md-3">&nbsp;</div>
													<div class="col-md-7" style="text-align: center; ">
														<hr style="border-top: 2px solid #888 !important;">
													</div>
													<div class="col-md-2">
														&nbsp;
													</div>
												</div>

												<div class="col-md-4"></div>
												<div class="col-md-8">
													<div class="col-md-5" style="text-align: right;">
														<h4>Sueldo Cobertura:</h4>
													</div>
													<div class="col-md-3">
														<input type="text" class="form-control move" style="width: 100%;" id="salario_minimo">
													</div>
													<div class="col-md-3">
														<h5>(Recomendado: $<span>1750</span>)</h5>
													</div>
												</div>

												<div class="col-md-4"></div>
												<div class="col-md-8">
													<div class="col-md-5">&nbsp;</div>
													<div class="col-md-3" style="text-align: center;">
														<h3 style="margin:0px;" id="signo_deducciones">+</h3>
													</div>
													<div class="col-md-3">
														&nbsp;
													</div>
												</div>

												<div class="col-md-4"></div>
												<div class="col-md-8">
													<div class="col-md-5" style="text-align: right;">
														<h4>Total Deducciones:</h4>
													</div>
													<div class="col-md-3">
														<input type="text" class="form-control move" style="width: 100%;" id="deducciones">
													</div>
													<div class="col-md-3">
														<h5 id="sentido_deducciones"></h5>
													</div>
												</div>

												<div class="col-md-4"></div>
												<div class="col-md-8">
													<div class="col-md-5">&nbsp;</div>
													<div class="col-md-3" style="text-align: center;">
														<h3 style="margin:0px;">+</h3>
													</div>
													<div class="col-md-3">
														&nbsp;
													</div>
												</div>

												<div class="col-md-1"></div>
												<div class="col-md-3" style="padding: 0px;">
													<div class="col-md-5" style="text-align: center; padding: 0px;">
														<h5 style="margin-bottom:4px;margin-top:0px;">Prod Alc - Prod Min:</h5>
														<input class="form-control move" style="width: 100%;" type="text" id="prod_excedente" readonly>
													</div>
													<div class="col-md-2" style="text-align: center;">
														<h4>x</h4>
													</div>
													<div class="col-md-5" style="text-align: center; padding: 0px;">
														<h5 style="margin-bottom:4px;margin-top:0px;">Porcentaje</h5>
														<select class="form-control move" style="width: 100%;" id="porcentaje_comisiones">
															<option value="0">--</option>
															<option value="12">12%</option>
															<option value="13">13%</option>
														</select>
													</div>
												</div>
												<div class="col-md-8">
													<div class="col-md-1" style="text-align: right;">
														<h3 style="margin-top:10px;">=</h3>
													</div>
													<div class="col-md-4" style="text-align: right;">
														<h4>Comisiones:</h4>
													</div>
													<div class="col-md-3">
														<input type="text" class="form-control move" style="width: 100%;" id="deducciones">
													</div>
													<div class="col-md-3">
														<h5 id="sentido_deducciones"></h5>
													</div>
												</div>

												<div class="col-md-4"></div>
												<div class="col-md-8">
													<div class="col-md-1">&nbsp;</div>
													<div class="col-md-11" style="text-align: center; ">
														<hr style="border-top: 3px solid #888 !important;">
													</div>
												</div>

												<div class="col-md-4"></div>
												<div class="col-md-8">
													<div class="col-md-5" style="text-align: right;">
														<h4><b>Salario Total p/Servicios:</b></h4>
													</div>
													<div class="col-md-3">
														<input type="text" class="form-control move" style="width: 100%;" id="total_servicios_sug" onchange="sumar_conceptos()">
													</div>
													<div class="col-md-3">
														<h5>Total sugerido: $<span id="total_servicios_sug"></span></h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- PANEL SERVICIOS -->
								<hr>

								<!-- PANEL INCENTIVOS -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTwo">
										<button id="btn_panel_incentivos" style="background-color: #34bf3e;" class="btn_dropdown"  data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
											INCENTIVOS
										</button>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
										<div class="panel-body" style="border: 0px;">
											<div class="col-md-1"></div>
											<div class="col-md-4" style="padding:0px;">
												<div class="col-md-6">
													<input type="checkbox" name="incentivos_op" id="inc_1" onchange="sumar_incentivos()"><span> CHECKLIST: <b id="porcentaje_checklist"></b>%</span>
												</div>
												<div class="col-md-6">
													<input type="checkbox" name="incentivos_op" id="inc_2" onchange="sumar_incentivos()"><span> PUNTUALIDAD CARGA</span>
												</div>
												<div class="col-md-6" style="padding-right: 0px !important;">
													<input type="checkbox" name="incentivos_op" id="inc_3" onchange="sumar_incentivos()"><span> PUNTUALIDAD DESC.</span>
												</div>
												<div class="col-md-6">
													<input type="checkbox" name="incentivos_op" id="inc_4" onchange="sumar_incentivos()"><span> VELOCIDAD 95km/h </span>
												</div>
												<div class="col-md-6">
													<input type="checkbox" name="incentivos_op" id="inc_5" onchange="sumar_incentivos()"><span> UNIFORME</span>
												</div>
												<div class="col-md-6">
													<input type="checkbox" name="incentivos_op" id="inc_6" onchange="sumar_incentivos()"><span> 0 INCIDENCIAS</span>
												</div>
												<div class="col-md-6">
													<input type="checkbox" name="incentivos_op" id="inc_7" onchange="sumar_incentivos()"><span> OTROS</span>
												</div>
											</div>
											<div class="col-md-7">
												<div class="col-md-4">
												</div>
												<div class="col-md-3" style="text-align: center;">
													<h3 style="margin-top:0px; margin-bottom: 10px;">+</h3>
												</div>
											</div>
											<div class="col-md-7">
												<div class="col-md-4" style="text-align: right;">

													<h4><b>Total aplicado: </b></h4>
												</div>
												<div class="col-md-3">
													<input type="text" id="total_incentivos" onchange="sumar_conceptos()" class="form-control" style="width: 100%; font-size: 18px;">
												</div>
												<div class="col-md-5">
													<h5>Total sugerido: $<span id="total_incentivos_sug"></span> </h5>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- PANEL INCENTIVOS -->
								<hr>

								<!-- PANEL TOTAL -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingThree">
										<button id="btn_panel_total" style="background-color: #47b4d4;" class="btn_dropdown" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
											TOTAL
										</button>
									</div>
									<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
										<div class="panel-body" style="border: 0px;">
											<div class="col-md-1"></div>
											<div class="col-md-4" style="padding:0px;">
												<h4>NOTAS</h4>
												<textarea class="form-control" id="notas_nomina_op" style="padding: 7px;width: 100%; border: 1px solid #ccc; height: 120px; border-radius: 3px; cursor:pointer;"></textarea>
											</div>
											<div class="col-md-7">
												<div class="col-md-11" style="text-align: center; ">
													<hr style="border-top: 4px solid #888 !important;">
												</div>
												<div class="col-md-1">
													&nbsp;
												</div>
											</div>

											<div class="col-md-7" style="text-align: center; ">
												<div class="col-md-3"></div>
												<div class="col-md-5">
													<input type="text" class="form-control" id="nomina_total" style="height:36px; font-size: 22px; width: 100%;">
												</div>
												<div class="col-md-3"></div>
											</div>
											<div class="col-md-7" style="text-align: center; ">
												<div class="col-md-3"></div>
												<div class="col-md-5" style="text-align: center;">
													<h3 style="margin-top: 5px; margin-bottom: 10px;"><b>Pago Final</b></h3>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- PANEL TOTAL -->

							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-md-6"></div>
						<div class="col-md-2 text-center">
						</div>
						<div class="col-md-2 text-center">
							<button type="button" class="btn btn-success" style="width:100%;" value="guardar" id="guardar_nomina_op">Pagar</button>
						</div>
						<div class="col-md-2 text-center">
							<button type="button" class="btn btn-danger" data-dismiss="modal" style="width:100%;">Cancelar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>







<div class="swal2-container swal2-center swal2-backdrop-show" style="overflow-y: auto; display:none;">
        <div aria-labelledby="" aria-describedby="" class="swal2-popup swal2-modal swal2-icon-error swal2-show" tabindex="" role="dialog" aria-live="" aria-modal="" style="display: flex;">
            <div class="swal2-header">
                <ul class="swal2-progress-steps" style="display: none;"></ul>
                <div class="swal2-icon swal2-error swal2-icon-show" style="display: flex;"><span class="swal2-x-mark">
                        <span class="swal2-x-mark-line-left"></span>
                        <span class="swal2-x-mark-line-right"></span>
                    </span>
                </div>
                <div class="swal2-icon swal2-question" style="display: none;">
                </div>
                <div class="swal2-icon swal2-warning" style="display: none;">
                </div>
                <div class="swal2-icon swal2-info" style="display: none;">
                </div>
                <div class="swal2-icon swal2-success" style="display: none;">
                </div>
                <img class="swal2-image" style="display: none;">
                <h2 class="swal2-title" id="swal2-title" style="display: flex;">Oops...</h2>
                <button type="button" class="swal2-close" aria-label="Close this dialog" style="display: none;">×</button>
            </div>
            <div class="swal2-content">
                <div id="swal2-content" class="swal2-html-container" style="display: block;">Necesitas llenar los campos!</div>
                <input class="swal2-input" style="display: none;"><input type="file" class="swal2-file" style="display: none;">
                <div class="swal2-range" style="display: none;">
                    <input type="range">
                    <output></output>
                </div>
                <select class="swal2-select" style="display: none;"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;">
                    <input type="checkbox"><span class="swal2-label"></span>
                </label>
                <textarea class="swal2-textarea" style="display: none;"></textarea>
                <div class="swal2-validation-message" id="swal2-validation-message">

                </div>
            </div>
            <div class="swal2-actions">
                <button type="button" class="swal2-confirm swal2-styled" aria-label="" style="display: inline-block; border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);">OK</button>
                <button type="button" class="swal2-cancel swal2-styled" aria-label="" style="display: none;">Cancel</button>
            </div>
            <div class="swal2-footer" style="display: none;"></div>
            <div class="swal2-timer-progress-bar-container">
                <div class="swal2-timer-progress-bar" style="display: none;">
                </div>
            </div>
        </div>
	</div>
	


	

<script src="../js/funciones_principal.js"></script>