<!DOCTYPE HTML>

<html>
	<head>
		<title>SOFTWORK</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../css/styles.css"/>
		<!-- Font Icon -->

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
		

		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		
		<script src="../js/sweetalert2.all.min.js"></script>
		<script src="../js/loadings.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<style>
		.booking-form{
			border-top-right-radius: 0px !important; 
			border-bottom-right-radius: 0px !important;
			border-top-left-radius: 7px !important; 
     		border-bottom-left-radius: 7px !important;
		}
		.form-input{
			margin: 15px 0px !important;
			padding-bottom: 35px !important;
		}
		.header_reset{
			padding: 20px 0px;
			margin-bottom: 20px;
		}
		.vertify-booking{
			padding: 25px 10px 35px 10px;
		}
	</style>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.php" class="logo">SOFTWORK</a>
					<nav id="nav">
						<a href="index.php">HOME</a>
						<a href="./registro.php">Registrarme</a>
					</nav>
				</div>
			</header>
			<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

		<!-- Banner -->
		
			<section class="banner" style="padding-top: 0px; padding-bottom:0px;">
				<div class="inner" style="padding:0px; max-width:100%">
					<div class="main" style="padding: 80px 0px;" >

						<!-- Registro parte 2-->
						<div id="registro_form2">
							<div class="container" style="padding:0px;">
								<div class="booking-content">
									<div class="booking-form login-form" style="width:40%" id="panel_reset_pswd">
											<div id="booking-form" >
												<div style="border-bottom: solid 2px #8c0303; width:auto;">
													<h2 style="margin: 0px !important; text-align:left; width: auto !important;">RECUPERAR MI CONTRASEÑA</h2>
												</div>
												<br>
												<h4 class="header_reset">Ingresa tu correo para recuperar su contraseña</h4>
												<div class="form-group form-input">
													<input type="email" id="correo_reset_pswd" required/>
													<label class="form-label">Email</label>
												</div>
												<div class="group">
													<button style="margin: 20px 0px !important;" class="button" id="submit_email">Aceptar</button>
													<a href="../index.php" class="vertify-booking">Iniciar Sesion!</a>
												</div>
											</div>
									</div>
									<div class="booking-image" style="width:60%;">
									</div>
								</div>
							</div>

						</div> 
						<!-- Registro parte 2-->

					</div>
				</div>
			</section>    



		<!-- Footer -->
			<section id="footer">
				<div class="inner">
					
					<div class="copyright">
						&copy; SOFTWORK 2020</a>
					</div>
				</div>
			</section>

	<script src="../js/funciones_resetpswd.js"></script>

	</body>
</html>