<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SOFTWORK</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- Font Icon -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

    <script src="../js/sweetalert2.all.min.js"></script>
    <script src="../js/loadings.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- SCROLL TABLE CON FILTRADO -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.css" />
    <!-- SCROLL TABLE CON FILTRADO -->
    <!-- SCROLL TABLE CON FILTRADO -->
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.js"></script>
    <!-- SCROLL TABLE CON FILTRADO -->

</head>

<style>
    .form-label {
        color: #666 !important;
    }

    textarea:focus+.form-label,
    textarea:valid+.form-label,
    input:focus+.form-label,
    input:valid+.form-label,
    select:focus+.form-label,
    select:valid+.form-label {
        top: 1px !important;
        font-size: 18px !important;
        ;
        color: rgba(18, 77, 107, 0.959) !important;
    }

    textarea:focus,
    textarea:valid,
    input:focus,
    input:valid,
    select:focus,
    select:valid {
        border-bottom: 1px solid rgba(18, 77, 107, 0.959) !important;
    }


    textarea,
    input,
    select {
        color: #555 !important;
        border-bottom: 1px solid #bbb !important;
    }

    input[type="date"] {
        width: 100%;
        white-space: pre;
        cursor: pointer;
    }

    input[type="date"]::before {
        content: attr(placeholder)"                                                            ";
    }

    /* Cuando se enfoca que no esten los espacios en blanco */
    input[type="date"]:focus::before {
        content: "" !important;
    }
</style>
<?php
if (!isset($_SESSION)) {
    session_start();
}
error_reporting(0);

if ($_SESSION['id'] != null) {

?>

    <script>
        /*VARIABLES GLOBALES*/
        var id_usuario = '<?php echo $_SESSION['id']; ?>';
        var nombre_usuario_loggeado = '<?php echo $_SESSION['nombre']; ?>';
        var correo_usuario = '<?php echo $_SESSION['correo']; ?>';
    </script>

    <body>

        <!-- Header -->
        <header id="header">
            <div class="inner">
                <a href="./explorar.php" class="logo">SOFTWORK</a>
                <nav id="nav">
                    <a href="./explorar.php">EXPLORAR</a>
                    <div class="dropdown">
                        <div class="dropbtn">USUARIO</div>
                        <div class="dropdown-content">
                            <a class="a_usuario" href="./perfil.php">Mi Perfil</a>
                            <a class="a_usuario" href="../index.php">Cerrar sesion</a>
                        </div>
                    </div>
                </nav>
            </div>
        </header>
        <a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>


        <!-- Banner Detalle Proyecto -->
        <section class="banner" id="banner_detalle_proyecto" style="padding-top: 0px; padding-bottom:0px;">
            <div class="inner" style="padding:0px; max-width:100%">
                <div class="main" style="padding: 0px 0px;">
                    <div class="col-md-12" style="padding: 60px 10px;">
                        <div class="row">
                            <div class="col-md-3 col-sm-12" style="margin:0px;">
                                <div class="row" style="width: 95% !important; padding: 25px 40px 25px 0px; border-right: 1px solid #ccc;">
                                    <div class="col-md-12">
                                        <img class="imagen_feed" src="../images/avatar-01.jpg" id="avatar_usuario">
                                    </div>
                                    <div class="col-md-12">
                                        <br>
                                    </div>
                                    <div class="col-md-12" style="text-align:center;">
                                        <h5 id="nombre_usuario" style="margin:0px; color: #444;"></h5>
                                        <h5 id="correo_usuario" style="margin:0px; color: #777; font-size:15px;"></h5>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="col-md-12 menu_perfil" id="menu_informacion">
                                        INFORMACION
                                    </div>
                                    <div class="col-md-12 menu_perfil" id="menu_proyectos">
                                        MIS PROYECTOS
                                    </div>
                                    <div class="col-md-12 menu_perfil" id="menu_propuestas" style="border-bottom: 1px solid #ccc;">
                                        MIS PROPUESTAS
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-12" style="margin:0px; text-align:center;">
                                <!-- PANEL INFORMACION PERSONAL -->
                                <div id="panel_informacion">
                                    <div class="row" style="padding: 30px; text-align:center;width: 100%; border-radius:5px;">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h3 style="color: #444;">EDITAR PERFIL</h3>
                                            <hr>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="text-align: left;">
                                            <h5 style="font-weight: 600; color: #333;">INFORMACION PERSONAL</h5>
                                            <hr class="hr_small_header">
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group form-input">
                                                <input maxlength="80" type="text" id="act_nombre" required />
                                                <label class="form-label">Nombre</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group form-input">
                                                <input maxlength="80" type="text" name="name" id="act_apellidos" required />
                                                <label for="name" class="form-label">Apellidos</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group form-input">
                                                <input type="date" id="act_fecha_nacimiento" required />
                                                <label class="form-label">Fecha de nacimiento</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group form-input">
                                                <input maxlength="20" type="text" id="act_telefono" required />
                                                <label class="form-label">Telefono</label>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group form-input">
                                                <select id="act_genero" required>
                                                    <option value="1">Hombre</option>
                                                    <option value="2">Mujer</option>
                                                    <option value="3">Prefiero no mencionarlo</option>
                                                </select>
                                                <label class="form-label" style="top: 1px; font-size: 11px; color: #edb77d;">Género</label>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-12">
                                            <div class="form-group form-input">
                                                <input maxlength="150" type="text" id="act_direccion" required />
                                                <label class="form-label">Direccion</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group form-input">
                                                <select name="" id="act_estado" required>
                                                    <option value="1" selected>Queretaro</option>
                                                    <option value="2">Guanajuato</option>
                                                    <option value="3">CDMX</option>
                                                </select>
                                                <label class="form-label" style="top: 1px; font-size: 11px; color: #edb77d;">Estado</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group form-input">
                                                <select id="act_municipio" required>
                                                    <option value="1">Santiago de Queretaro</option>
                                                    <option value="2">Cadereyta</option>
                                                    <option value="3" selected>San Juan del Rio</option>
                                                </select>
                                                <label class="form-label" style="top: 1px; font-size: 11px; color: #edb77d;">Municipio</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group form-input">
                                                <textarea id="act_descripcion_personal" cols="30" rows="3" maxlength="150" onkeyup="countChars4(this);" required></textarea>
                                                <label class="form-label">Breve Descripcion Personal</label>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <br>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="text-align: left;">
                                            <h5 style="font-weight: 600; color: #333;">INFORMACION PROFESIONAL</h5>
                                            <hr class="hr_small_header">
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group form-input">
                                                <select id="act_grado_academico" required>
                                                    <option value="1">Secundaria</option>
                                                    <option value="2">Preparatoria / Bachillerato</option>
                                                    <option value="3" selected>Universidad / Licenciatura</option>
                                                    <option value="4">Maestria</option>
                                                    <option value="5">Doctorado</option>
                                                </select>
                                                <label class="form-label" style="top: 1px; font-size: 11px; color: #edb77d;">Grado Academico</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group form-input">
                                                <input maxlength="80" type="text" name="name" id="act_carrera" required />
                                                <label for="name" class="form-label">Carrera / Especialidad</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group form-input">
                                                <textarea id="act_experiencia_profesional" cols="30" rows="3" maxlength="150" onkeyup="countChars5(this);" required></textarea>
                                                <label class="form-label">Experiencia Profesional</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12"><br></div>
                                        <div class="col-md-9 col-sm-9"></div>
                                        <div class="col-md-3 col-sm-3">
                                            <button type="button" class="button" id="actualizar_datos" style="width:100%;">ACTUALIZAR</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- PANEL INFORMACION PERSONAL -->

                                <!-- PANEL MIS PROYECTOS -->
                                <div id="panel_proyectos" style="display:none;">
                                    <div class="row" style="padding: 30px; text-align:center;width: 100%; border-radius:5px;">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h3 style="color: #444;">MIS PROYECTOS</h3>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <button id="crear_nuevo_proyecto" type="button" class="button" id="actualizar_datos" style="width:100%;">NUEVO PROYECTO</button>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <br>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <hr>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <h5 id="header_proy_activos" class="header_proyectos header_selected" onclick="show_proyectos_activos()">ACTIVOS</h5>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <h5 id="header_proy_proceso" class="header_proyectos" onclick="show_proyectos_proceso()">EN PROCESO</h5>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <h5 id="header_proy_finalizados" class="header_proyectos" onclick="show_proyectos_finalizados()">FINALIZADOS</h5>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <h5 id="header_proy_inactivos" class="header_proyectos" onclick="show_proyectos_inactivos()">INACTIVOS</h5>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <br>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row" id="proyectos_container_activos">
                                                <div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto">
                                                    <div class="panel_container">
                                                        <div style="width: 100%; text-align:left; height:75%; padding: 10px 20px;">
                                                            <h3 style="color: #222 !important;"><b>PROYECTO</b></h3>
                                                            <h5 style="color: #555 !important;">Aplicacion movil para niños de preescolar</h5>
                                                        </div>
                                                        <div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">
                                                            <button type="button" class="button ver_mas">Ver Mas</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="proyectos_container_proceso" style="display:none;">
                                            </div>
                                            <div class="row" id="proyectos_container_finalizados" style="display:none;">
                                            </div>
                                            <div class="row" id="proyectos_container_inactivos" style="display:none;">
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12"><br></div>
                                    </div>
                                </div>
                                <!-- PANEL MIS PROYECTOS -->

                                <!-- PANEL CREAR PROYECTO -->
                                <div id="panel_crear_proyecto" style="display:none;">
                                    <div class="row" style="padding: 30px; text-align:center;width: 100%; border-radius:5px;">
                                        <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: left; padding: 15px;">
                                            <button type="button" class="button btn-regresar" id="regresar_panel_mis_proyectos2" style="font-weight: 700 !important;color:#222 !important; background-color: #ffbc00c4;"><i class="fas fa-arrow-circle-left fa-lg"></i> REGRESAR</button>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h3 style="color: #444;">NUEVO PROYECTO</h3>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <hr>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <br>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group form-input">
                                                <input maxlength="50" type="text" id="titulo_nuevo_proyecto" required />
                                                <label class="form-label">Titulo del Proyecto</label>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group form-input">
                                                <input type="date" id="fecha_entrega_nuevo_proyecto" value="05/10/2020" required />
                                                <label class="form-label" style="color:#666">Fecha Tentativa de Entrega</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <br>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group form-input">
                                                <textarea id="descripcion_nuevo_proyecto" cols="30" rows="5" maxlength="5000" onkeyup="countChars(this);" required></textarea>
                                                <label class="form-label">Descripcion del Proyecto</label>
                                            </div>
                                            <div class="form-group form-input" style="padding-top: 0px; font-size:10px; text-align: end; color:#888;">
                                                <label id="contador_descripcion">0/5000</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group form-input">
                                                <textarea id="restricciones_nuevo_proyecto" cols="30" rows="5" maxlength="5000" onkeyup="countChars2(this);" required></textarea>
                                                <label class="form-label">Restricciones del Proyecto</label>
                                            </div>
                                            <div class="form-group form-input" style="padding-top: 0px; font-size:10px; text-align: end; color:#888;">
                                                <label id="contador_restricciones">0/5000</label>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-12">
                                            <div class="form-group form-input">
                                                <textarea id="observaciones_nuevo_proyecto" cols="30" rows="4" maxlength="5000" onkeyup="countChars3(this);" required></textarea>
                                                <label class="form-label">Observaciones Tecnicas</label>
                                            </div>
                                            <div class="form-group form-input" style="padding-top: 0px; font-size:10px; text-align: end; color:#888;">
                                                <label id="contador_observaciones">0/5000</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group form-input">
                                                        <input type="number" id="presupuesto_min_nuevo_proyecto" required />
                                                        <label class="form-label">Presupuesto Min. $</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group form-input">
                                                        <input type="text" id="presupuesto_max_nuevo_proyecto" required />
                                                        <label class="form-label">Presupuesto Max. $</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                            <br>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                            <hr>
                                            <br>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="text-align: center;">
                                            <button type="button" class="button" id="publicar_proyecto">PUBLICAR PROYECTO</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- PANEL CREAR PROYECTO -->

                                <!-- PANEL DETALLE PROYECTO -->
                                <div id="panel_detalle_proyecto" style="display:none;">
                                    <div class="row" style="padding: 30px; padding-top:0px; text-align:center;width: 100%; border-radius:5px;">
                                        <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: left; padding: 15px;">
                                            <button type="button" class="button btn-regresar" id="regresar_panel_mis_proyectos" style="font-weight: 700 !important;color:#222 !important; background-color: #ffbc00c4;"><i class="fas fa-arrow-circle-left fa-lg"></i> REGRESAR</button>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h3 id="titulo_proyecto_detalle" style="color: #444;">APP</h3>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <hr style="margin-left: 10%; width:80%;">
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <h4 id="header_detalle_proyecto" class="header_proyectos header_selected" onclick="show_info_proyecto()">DETALLES DEL PROYECTO</h4>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <h4 id="header_propuestas_proyecto" class="header_proyectos" onclick="show_propuestas_proyecto()">PROPUESTAS</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <br>
                                            <br>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row" id="view_info_proyecto">
                                                <div class="row" id="btn_accion_proyecto" style="width: 100%;">
                                                </div>
                                                
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                    <br>
                                                </div>
                                                <div class="col-md-2">
                                                    <br>
                                                </div>

                                                <div class="col-md-4 col-sm-12 col-xs-12" style="text-align:left;">
                                                    <h5 style="font-weight: 600; color: #333;">FECHA DE ENTREGA</h5>
                                                    <hr class="hr_small_header">
                                                    <div id="fecha_entrega_proyecto_detalle" style="width:100% !important; min-height: 50px; color:#444;"></div>
                                                </div>
                                                <div class="col-md-1">
                                                    <br>
                                                </div>
                                                <div class="col-md-5 col-sm-12 col-xs-12" style="text-align:left;">
                                                    <h5 style="font-weight: 600; color: #333;">PRESUPUESTO</h5>
                                                    <hr class="hr_small_header">
                                                    <div id="presupuesto_proyecto_detalle" style="width:100% !important; min-height: 50px; color:#444;"></div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:left;">
                                                    <h5 style="font-weight: 600; color: #333;">DESCRIPCION DEL PROYECTO</h5>
                                                    <hr class="hr_small_header">
                                                    <div id="descripcion_proyecto_detalle" style="width:100% !important; min-height: 50px; color:#444;"></div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>
                                                <div class="col-md-6 col-sm-12" style="text-align:left;">
                                                    <h5 style="font-weight: 600; color: #333;">OBSERVACIONES TECNICAS</h5>
                                                    <hr class="hr_small_header">
                                                    <div id="observaciones_proyecto_detalle" style="width:100% !important; min-height: 50px; color:#444;"></div>

                                                </div>
                                                <div class="col-md-6 col-sm-12" style="text-align:left;">
                                                    <h5 style="font-weight: 600; color: #333;">RESTRICCIONES</h5>
                                                    <hr class="hr_small_header">
                                                    <div id="restricciones_proyecto_detalle" style="width:100% !important; min-height: 50px; color:#444;"></div>

                                                </div>
                                                <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                    <br>
                                                </div>
                                                <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                    <hr>
                                                    <br>
                                                </div>
                                            </div>

                                            <div class="row" id="view_propuestas_proyecto" style="display: none;">

                                                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                                                    <h5 style="font-weight: 600; color: #333;">PROPUESTA ACEPTADA</h5>
                                                    <hr class="hr_small_header">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12" id="panel_propuesta_aceptada">
                                                    <input type="hidden" id="id_propuesta_aceptada">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                                                    <h5 style="font-weight: 600; color: #333;">PROPUESTAS</h5>
                                                    <hr class="hr_small_header">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="row" style="width: 100%;">
                                                        <table id="table_propuestas" class="table table-fixed" style="width:100%; border:0px; margin-top:0px !important">
                                                            <thead class="thead-dark">
                                                                <tr>
                                                                    <th class="col-md-2 col-xs-2">USUARIO</th>
                                                                    <th class="col-md-2 col-xs-2">FECHA</th>
                                                                    <th class="col-md-4 col-xs-4">DESCRIPCION</th>
                                                                    <th class="col-md-2 col-xs-2">COTIZACION</th>
                                                                    <th class="col-md-2 col-xs-2">STATUS</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="body_table_propuestas">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                    <hr>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- PANEL DETALLE PROYECTO -->

                                <!-- PANEL MIS PROPUESTAS -->
                                <div id="panel_propuestas" style="display:none;">
                                    <div class="row" style="padding: 30px; text-align:center;width: 100%; border-radius:5px;">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h3 style="color: #444;">MIS PROPUESTAS</h3>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                            <br>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <hr>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <h5 id="header_prop_activas" class="header_proyectos header_selected" onclick="show_propuestas_activas()">ACTIVAS</h5>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <h5 id="header_prop_aceptadas" class="header_proyectos" onclick="show_propuestas_aceptadas()">ACEPTADAS/FINALIZADAS</h5>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <h5 id="header_prop_rechazadas" class="header_proyectos" onclick="show_propuestas_rechazadas()">RECHAZADAS</h5>
                                                </div>
                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <br>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row" id="propuestas_container_activas">
                                                <div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto">
                                                    <div class="panel_container">
                                                        <div style="width: 100%; text-align:left; height:75%; padding: 10px 20px;">
                                                            <h3 style="color: #222 !important;"><b>PROYECTO</b></h3>
                                                            <h5 style="color: #555 !important;">Aplicacion movil para niños de preescolar</h5>
                                                        </div>
                                                        <div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">
                                                            <button type="button" class="button ver_mas">Ver Mas</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="propuestas_container_aceptadas" style="display:none;">
                                            </div>
                                            <div class="row" id="propuestas_container_rechazadas" style="display:none;">
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12"><br></div>
                                    </div>
                                </div>
                                <!-- PANEL MIS PROPUESTAS -->

                                <!-- PANEL DETALLE PROPUESTA -->
                                <div id="panel_detalle_propuesta" style="display:none;">
                                    <div class="row" style="padding: 30px; padding-top:0px; text-align:center;width: 100%; border-radius:5px;">
                                        <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: left; padding: 15px;">
                                            <button type="button" class="button btn-regresar" id="regresar_panel_mis_propuestas" style="font-weight: 700 !important;color:#222 !important; background-color: #ffbc00c4;"><i class="fas fa-arrow-circle-left fa-lg"></i> REGRESAR</button>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h3 id="titulo_propuesta_detalle" style="color: #444;">INFORMACION DE MI PROPUESTA</h3>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <hr style="margin-left: 10%; width:80%;">
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row" id="view_info_proyecto">
                                                <div class="row" id="btn_accion_propuesta" style="width: 100%;">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                                                    <h5 style="font-weight: 600; color: #333;">MI PROPUESTA</h5>
                                                    <hr class="hr_small_header">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12" id="panel_mi_propuesta">
                                                    <input type="hidden" id="id_mi_propuesta">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <hr style="height: 2px">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                                                    <h5 style="font-weight: 600; color: #333;">PARA EL PROYECTO...</h5>
                                                    <hr class="hr_small_header">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                                                    <div id="titulo_proyecto_propuesta" style="width: 100%; min-height:20px; color:#444; font-size: 20px !important;"></div>
                                                    <hr style="width: 80%; margin-left: 10%; height: 2px;">
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>
                                                <div class="col-md-2">
                                                    <br>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12" style="text-align:left;">
                                                    <h5 class="titulos_proyecto">FECHA DE ENTREGA</h5>
                                                    <div id="fecha_entrega_proyecto_propuesta" style="width:100% !important; min-height: 50px; color:#444;"></div>
                                                </div>
                                                <div class="col-md-1">
                                                    <br>
                                                </div>
                                                <div class="col-md-5 col-sm-12 col-xs-12" style="text-align:left;">
                                                    <h5 class="titulos_proyecto">PRESUPUESTO</h5>
                                                    <div id="presupuesto_proyecto_propuesta" style="width:100% !important; min-height: 50px; color:#444;"></div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:left;">
                                                    <h5 class="titulos_proyecto">DESCRIPCION DEL PROYECTO</h5>
                                                    <div id="descripcion_proyecto_propuesta" style="width:100% !important; min-height: 50px; color:#444;"></div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <br>
                                                </div>
                                                <div class="col-md-6 col-sm-12" style="text-align:left;">
                                                    <h5 class="titulos_proyecto">OBSERVACIONES TECNICAS</h5>
                                                    <div id="observaciones_proyecto_propuesta" style="width:100% !important; min-height: 50px; color:#444;"></div>

                                                </div>
                                                <div class="col-md-6 col-sm-12" style="text-align:left;">
                                                    <h5 class="titulos_proyecto">RESTRICCIONES</h5>
                                                    <div id="restricciones_proyecto_propuesta" style="width:100% !important; min-height: 50px; color:#444;"></div>

                                                </div>
                                                <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                    <br>
                                                </div>
                                                <div class="col-md-12 col-sm-12" style="text-align: center;">
                                                    <hr>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- PANEL DETALLE PROYECTO -->

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- Banner Detalle Proyecto -->

        <!-- Footer -->
        <section id="footer">
            <div class="inner">
                <div class="copyright">
                    &copy; Untitled Design: <a href="https://templated.co/">TEMPLATED</a>. Images <a href="https://unsplash.com/">Unsplash</a>
                </div>
            </div>
        </section>

        <!-- MODALES MODALES MODALES MODALES MODALES MODALES-->
        <div class="modal-swal modal fade" id="modal_detalle_propuesta" role="dialog" style="overflow-y: auto !important; display:none;">
            <div aria-labelledby="exampleModalLabel" class="modal-dilog modal-lg swal2-show" tabindex="" role="dialog" style="display:flex; width: 100% !important;">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center;">
                        <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Detalles Propuesta</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="descripcion_proyecto_detalle" style="width:100% !important; min-height: 50px; color:#444; padding:0px 15px;">
                            <input type="hidden" id="id_propuesta_detalle">
                            <div class="col-md-2 col-sm-4" style="text-align: center;">
                                <img style="height: 100px; width: auto;" class="imagen_feed" src="../images/avatar-01.jpg" id="avatar_usuario">
                            </div>
                            <div class="col-md-6 col-sm-8" style="padding-top: 20px;">
                                <h5 id="nombre_usuario_propuesta_detalle" style="font-weight: 600; color: #444;">Fernando González</h4>
                                    <h5 id="carrera_usuario_propuesta_detalle" style="color: #888;">Ingeniero de Software</h5>
                            </div>
                            <div class="col-md-4 col-sm-12" style="padding-left: 50px; padding-top:10px; border-left: 1px solid #cdcdcd">
                                <p class="titulos_proyecto">FECHA DE ENTREGA</p>
                                <div id="fecha_entrega_propuesta_detalle" style="width:100% !important; min-height: 30px; color:#444; font-weight:400"></div>
                                <p style="margin-bottom: 5px;" class="titulos_proyecto">COSTO FINAL</p>
                                <div id="presupuesto_propuesta_detalle" style="width:100% !important; min-height: 20px; color:#444; font-weight:400"></div>
                            </div>
                            <div class="col-md-7 col-sm-12">
                                <hr style="margin-bottom: 25px;">
                            </div>
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <p style="margin-bottom: 5px;" class="titulos_proyecto">DESCRIPCIÓN DE LA PROPUESTA</p>
                                <div id="descripcion_propuesta_detalle" style="width:100% !important; min-height: 60px; color:#444;"></div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <p style="margin-bottom: 5px;" class="titulos_proyecto">ESPECIFICACIONES TECNICAS</p>
                                <div id="especificaciones_propuesta_detalle" style="width:100% !important; min-height: 40px; color:#444;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="swal2-actions" style="margin: 0px !important;" id="footer_modal">
                            <button type="button" id="rechazar_propuesta" class="btn btn-danger swal2-styled" style="display: inline-block;">Rechazar</button>
                            <button type="button" id="aceptar_propuesta" class="btn btn-success swal2-styled" style="display: inline-block;">Confirmar</button>
                            <button type="button" data-dismiss="modal" id="close_modal_detalle_propuesta" class="btn btn-info swal2-styled">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="../js/funciones_perfil.js"></script>
        <script src="../js/peticiones_perfil.js"></script>
    </body>
<?php

} else { //Si no se cuenta con el acceso pertinente muestra el siguiente mensaje
    header("Location: ./401.php");
    //require 'views/401.php';
}
?>

</html>