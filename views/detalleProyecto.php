<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SOFTWORK</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- Font Icon -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    
    <script src="../js/sweetalert2.all.min.js"></script>
    <script src="../js/loadings.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

    <!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.php" class="logo">SOFTWORK</a>
					<nav id="nav">
						<a href="../index.php">EXPLORAR</a>
						<a href="./registro.php">USUARIO</a>
					</nav>
				</div>
			</header>
			<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>
            
        <!-- Banner Registro parte 1-->
            
                <section id="banner" style="padding-top: 0px; padding-bottom:0px;">
                    <div class="inner" style="padding:0px; max-width:100%">
                        <div class="main" style="padding: 0px 0px;">
                            <div class="col-md-12" style="padding: 60px 10px;">
                                <div class="row">
                                    <div class="col-md-8 col-sm-12" style="margin:0px;">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <h3 id="titulo_proyecto">Proyecto de Prueba</h3>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <hr>
                                            </div>
                                            
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-2 col-md-2">
                                                        <img class="imagen_feed" src="../images/avatar-01.jpg" id="avatar_usuario">
                                                    </div>
                                                    <div class="col-md-8 col-sm-8">
                                                        <h4 id="nombre_usuario">Fernando González</h4>
                                                        <h5 id="especialidad">Ingeniero de Software</h5>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <hr>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <h5>Descripcion del proyecto</h5>
                                                <div style="width:100% !important; min-height: 150px; background:red;"></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <br>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <h5>Observaciones técnicas</h5>
                                                <div style="width:100% !important; min-height: 150px; background:red;"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <h5>Restricciones</h5>
                                                <div style="width:100% !important; min-height: 150px; background:red;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3 col-sm-12" style="margin:0px; text-align:center;">
                                        <br>
                                        <div style="padding: 30px; text-align:center;width: 100%; border: 1px solid #ccc; border-radius:5px; background-color: rgba(5, 41, 59, 0.959);">
                                            <h1 style="font-size: 95px; margin: 0px;">5</h1>
                                            <h4>POPUESTAS</h4>
                                            <hr style="background-color: #edb77d; height: 2px; margin-top: 20px; margin-bottom:20px;" >
                                            <h5>PRESUPUESTO</h5>
                                            <p style="color:#ddf;">Minimo: <span id="presupuesto_minimo"></span></p>
                                            <p style="color:#ddf;">Máximo: <span id="presupuesto_maximo"></span></p>
                                            <hr style="background-color: #edb77d; height: 2px; margin-top: 20px; margin-bottom:20px;">
                                            <button class="button" style="width:100%;">CREAR PROPUESTA</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>    
            
        <!-- Banner -->


		<!-- Footer -->
			<section id="footer">
				<div class="inner">
					
					<div class="copyright">
						&copy; Untitled Design: <a href="https://templated.co/">TEMPLATED</a>. Images <a href="https://unsplash.com/">Unsplash</a>
					</div>
				</div>
            </section>
    <script src="../js/funciones_detalleProyecto.js"></script>
    <script src="../js/peticiones_detalleProyecto.js"></script>
</body>

</html>