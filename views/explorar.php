<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SOFTWORK</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- Font Icon -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

    <script src="../js/sweetalert2.all.min.js"></script>
    <script src="../js/loadings.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<?php
	if(!isset($_SESSION)){
	    session_start();
    }
    error_reporting(0);

    if($_SESSION['id'] != null){
	
?>

<script>
    /*VARIABLES GLOBALES*/
    var id_usuario = '<?php echo $_SESSION['id'];?>';
	var nombre_usuario_loggeado = '<?php echo $_SESSION['nombre'];?>';
    var correo_usuario = '<?php echo $_SESSION['correo'];?>';
    var sesion = '<?php echo $_SESSION['sesion'];?>';
    var date_session = new Date();
</script>

<style>
    h6{
        text-align: left;
        font-size: 16px;
    }

    .div_desempeño{
        width: 100%;
        padding: 15px 5px;
        margin: 5px 0px;
    }

    .contadores{
        padding: 5px 9px;
        border-radius: 7rem;
        border: 1px solid #ccc;
        color: white;
    }
</style>

<body>

    <!-- Header -->
    <header id="header">
        <div class="inner">
            <a href="./explorar.php" class="logo">SOFTWORK</a>
            <nav id="nav">
                <a href="./explorar.php">EXPLORAR</a>
                <div class="dropdown">
                    <div class="dropbtn">USUARIO</div>
                    <div class="dropdown-content">
                        <a class="a_usuario" href="./perfil.php">Mi Perfil</a>
                        <a class="a_usuario" style="cursor: pointer;" id="cerrar_sesion">Cerrar sesion</a>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

    <!-- Banner Panel Proyecos-->

    <section id="banner_proyectos" class="banner" style="padding-top: 0px; padding-bottom:0px;">
        <div class="inner" style="padding:0px; max-width:100%">
            <div class="main" style="padding: 0px 0px;">
                <div class="row">
                    <div class="col-12 text-center py-4">
                        <h1 id="proyectos_disponibles" style="color: #333; font-weight: 700;margin-top:2rem; margin-bottom:0px;">
                            PROYECTOS DISPONIBLES
                        </h1>
                    </div>
                </div>
                <hr style="margin-left: 10%; width:80%; margin-top: 0px; margin-bottom: 40px;" >
                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 60px;">
                    <div class="row">
                        <div class="col-lg-3 col-md-3" style="padding-left: 35px; padding-right:60px;">
                            <div style="padding: 30px; text-align:center;width: 100%; border: 1px solid #ccc; border-radius:5px; background-color: rgba(5, 41, 59, 0.959);">
                                <h3 style="font-weight:700">DESEMPEÑO</h3>
                                <hr style="background-color: #edb77d; height: 2px; margin-top: 20px; margin-bottom:20px;">
                                <h5>PROYECTOS</h5>
                                <div class="div_desempeño">
                                    <div style="width: 70%; float:left; text-align:left;">
                                        <span style="color: #00de8c !important">ACTIVOS: </span>
                                    </div>
                                    <div style="width: 30%; float:right; text-align:right">
                                        <span style="background-color: #00de8c !important" class="contadores" id="contador_proy_activos">6</span>
                                    </div>
                                </div>
                                <div class="div_desempeño">
                                    <div style="width: 70%; float:left; text-align:left;">
                                        <span style="color: #e7aa00 !important">EN PROCESO: </span>
                                    </div>
                                    <div style="width: 30%; float:right; text-align:right">
                                        <span style="background-color: #e7aa00 !important" class="contadores" id="contador_proy_proceso">6</span>
                                    </div>
                                </div>
                                <div class="div_desempeño">
                                    <div style="width: 70%; float:left; text-align:left;">
                                        <span style="color: #75bff3 !important">FINALIZADOS: </span>
                                    </div>
                                    <div style="width: 30%; float:right; text-align:right">
                                        <span style="background-color: #75bff3 !important" class="contadores" id="contador_proy_finalizados">6</span>
                                    </div>
                                </div>
                                <div class="div_desempeño">
                                    <div style="width: 70%; float:left; text-align:left;">
                                        <span style="color: #ff8560 !important">INACTIVOS: </span>
                                    </div>
                                    <div style="width: 30%; float:right; text-align:right">
                                        <span style="background-color: #ff8560 !important" class="contadores" id="contador_proy_inactivos">6</span>
                                    </div>
                                </div>
                                <hr style="background-color: #edb77d; height: 2px; margin-top: 35px; margin-bottom:25px;">
                                <h5>PROPUESTAS</h5>
                                <div class="div_desempeño">
                                    <div style="width: 70%; float:left; text-align:left;">
                                        <span style="color: #00de8c !important">ACTIVAS: </span>
                                    </div>
                                    <div style="width: 30%; float:right; text-align:right">
                                        <span style="background-color: #00de8c !important" class="contadores" id="contador_prop_activas">6</span>
                                    </div>
                                </div>
                                <div class="div_desempeño">
                                    <div style="width: 70%; float:left; text-align:left;">
                                        <span style="color: #e7aa00 !important">ACEPTADAS: </span>
                                    </div>
                                    <div style="width: 30%; float:right; text-align:right">
                                        <span style="background-color: #e7aa00 !important" class="contadores" id="contador_prop_aceptadas">6</span>
                                    </div>
                                </div>
                                <div class="div_desempeño">
                                    <div style="width: 70%; float:left; text-align:left;">
                                        <span style="color: #75bff3 !important">FINALIZADAS: </span>
                                    </div>
                                    <div style="width: 30%; float:right; text-align:right">
                                        <span style="background-color: #75bff3 !important" class="contadores" id="contador_prop_finalizadas">6</span>
                                    </div>
                                </div>
                                <div class="div_desempeño">
                                    <div style="width: 70%; float:left; text-align:left;">
                                        <span style="color: #ff8560 !important">RECHAZADAS: </span>
                                    </div>
                                    <div style="width: 30%; float:right; text-align:right">
                                        <span style="background-color: #ff8560 !important" class="contadores" id="contador_prop_rechazadas">6</span>
                                    </div>
                                </div>
                                <hr style="background-color: #edb77d; height: 2px; margin-top: 35px; margin-bottom:25px;">
                                <button type="button" class="button" style="width:100%;" onclick="crear_proyecto_explorar();">CREAR PROYECTO</button>
                            </div>
                        </div>
                        <div class="feed-container col-lg-9 col-md-9 col-sm-12" style="padding-left: 30px; padding-right:0px;" id="proyectos-container" >

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

    <!-- Banner -->

    <!-- Banner Detalle Proyecto -->
    <section class="banner" id="banner_detalle_proyecto" style="display: none;padding-top: 0px; padding-bottom:0px;">
        <div class="inner" style="padding:0px; max-width:100%">
            <div class="main" style="padding: 0px 0px;">
                <div class="col-md-12" style="padding: 30px 10px;">
                    <div class="row">
                        <div class="col-md-8 col-sm-12" style="margin:0px;">
                            <div class="row">

                                <div class="col-md-12 col-sm-12" style="text-align:left;">
                                    <button type="button" class="button" id="regresar_explorar" style="font-weight: 700 !important;color:#222 !important; background-color: #ffbc00c4;"><i class="fas fa-arrow-circle-left fa-lg"></i> REGRESAR</button>
                                </div>
                                <div class="col-md-12 col-sm-12" style="text-align:center;">
                                    <h3 style="color: #333; font-weight:700;" id="titulo_proyecto"></h3>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <hr>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <input type="hidden" id="id_proyecto">
                                    <div class="row">
                                        <div class="col-sm-2 col-md-2">
                                            <img class="imagen_feed" src="../images/avatar-01.jpg" id="avatar_usuario">
                                        </div>
                                        <div class="col-md-8 col-sm-8"> 
                                            <h4 style="font-weight: 600; color: #444;" id="nombre_usuario_proyecto">Fernando González</h4>
                                            <h5 style="color: #888;" id="especialidad_usuario">Ingeniero de Software</h5>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <hr>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <h5  style="font-weight: 600; color: #333;">DESCRIPCION DEL PROYECTO</h5>
                                    <hr class="hr_small_header">
                                    <div id="descripcion_proyecto" style="width:100% !important; min-height: 150px; color:#444;"></div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <br>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <h5  style="font-weight: 600; color: #333;">OBSERVACIONES TECNICAS</h5>
                                    <hr class="hr_small_header">
                                    <div id="observaciones_proyecto" style="width:100% !important; min-height: 150px; color:#444;"></div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <h5  style="font-weight: 600; color: #333;">RESTRICCIONES</h5>
                                    <hr class="hr_small_header">
                                    <div id="restricciones_proyecto" style="width:100% !important; min-height: 150px; color:#444;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3 col-sm-12" style="margin:0px; text-align:center;">
                            <br>
                            <div style="padding: 30px; text-align:center;width: 100%; border: 1px solid #ccc; border-radius:5px; background-color: rgba(5, 41, 59, 0.959);">
                                <h1 id="num_propuestas" style="font-size: 95px; margin: 0px;">5</h1>
                                <h4>POPUESTAS</h4>
                                <hr style="background-color: #edb77d; height: 2px; margin-top: 20px; margin-bottom:20px;">
                                <h5>PRESUPUESTO</h5>
                                <p style="font-weight: normal;color:#ddf;">Minimo: $ <b id="presupuesto_minimo"></b></p>
                                <p style="font-weight: normal;color:#ddf;">Máximo: $ <b id="presupuesto_maximo"></b></p>
                                <hr style="background-color: #edb77d; height: 2px; margin-top: 20px; margin-bottom:20px;">
                                <button type="button" class="button" id="crear_propuesta" style="width:100%;" data-toggle="modal" data-target="#modal_crear_propuesta" onclick="limpiar_modal_propuesta();">CREAR PROPUESTA</button>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Banner Detalle Proyecto -->


    
		<!-- Footer -->
    <section id="footer">
        <div class="inner">

            <div class="copyright">
                &copy; Untitled Design: <a href="https://templated.co/">TEMPLATED</a>. Images <a href="https://unsplash.com/">Unsplash</a>
            </div>
        </div>
    </section>

    <style>
        textarea:focus + .form-label, textarea:valid + .form-label,  input:focus + .form-label, input:valid + .form-label{
            top: 1px;
            font-size: 14px !important;;
            color: #f9a64d !important;
        }

        textarea, input{
            color: #444;
        }

        input[type="date"] {
            width: 100%;
            white-space: pre;
            cursor: pointer;
        }

        input[type="date"]::before{
            content: attr(placeholder)"                                                           ";
        }
        /* Cuando se enfoca que no esten los espacios en blanco */
        input[type="date"]:focus::before {
            content: "" !important;
        }
    
    </style>
    

    <!-- MODALES MODALES MODALES MODALES MODALES MODALES-->
    <div class="modal-swal modal fade" id="modal_crear_propuesta" role="dialog" style="overflow-y: auto !important; display:none;">
        <div aria-labelledby="exampleModalLabel" class="modal-dilog modal-lg swal2-show" tabindex="" role="dialog" style="display:flex; width: 100% !important;">
            <div class="modal-content">
                <div class="modal-header" style="text-align: center;">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Nueva Propuesta</h5>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding:10px;">
                        <div class="col-md-12">
                            <div class="form-group form-input">
                                <textarea id="descripcion_propuesta" cols="30" rows="5" style="width: 100%;" required></textarea>
                                <label class="form-label" style="color:#666">Descripcion de la Propuesta </label>
                            </div>
                        </div>
                        <div class="col-md-12"><br>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group form-input">
                                <textarea id="especificaciones_propuesta" cols="30" rows="5" style="width: 100%;" required></textarea>
                                <label class="form-label" style="color:#666">Especificaciones Tecnicas</label>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12"> 
                                    <div class="form-group form-input">
                                        <input type="date"  id="fecha_entrega_propuesta" value="05/10/2020" required/>
                                        <label class="form-label" style="color:#666" >Fecha de entrega</label>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group form-input">
                                        <input type="text" id="presupuesto_propuesta" value="" required />
                                        <label class="form-label" style="color:#666">Presupuesto</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <div class="swal2-actions" style="margin: 0px !important;">
                    <button type="button" id="guardar_propuesta" class="swal2-confirm swal2-styled" style="display: inline-block; border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);">Confirmar</button>
                    <button type="button" data-dismiss="modal" id="close_modal_propuesta" class="swal2-cancel swal2-styled" aria-label="" style="display: block;">Cancelar</button>
                </div>
                </div>
            </div>
        </div>
    </div>


    <script src="../js/funciones_principal.js"></script>
    <script src="../js/peticiones_principal.js"></script>
</body>

<?php
		
	}else{ //Si no se cuenta con el acceso pertinente muestra el siguiente mensaje
		header("Location:./401.php");
		//require 'views/401.php';
	}
 ?>
</html>