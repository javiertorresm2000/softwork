<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Document</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    
    <script src="../js/sweetalert2.all.min.js"></script>
</head>
<style>
    
</style>

<body>
    <div class="container-fluid text-white">
        <div class="row menu" style="margin-bottom:5rem;">
            <div class="col-sm-12 col-md-5 col-lg-5 align-self-center menu2">
                SOFTWORK
            </div>
            <div class="col-md-3 col-lg-3 d-none d-md-block align-self-center">
                <input type="text" placeholder="   Buscar Perfiles" id="BuscadorProyectos">
            </div>
            <div class="col-sm-6 col-md-2 col-lg-2 align-self-center">
                <a id="usuario" href=""><b>Explorar Proyectos</b></a>
            </div>
            <div class="col-sm-6 col-md-2 col-lg-2 align-self-center">
                <a id="usuario" href=""><b>Usuario</b></a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center py-4">
                <h1 class="display-4" style="margin-top:5rem;">
                    Proyectos disponibles
                </h1>
            </div>
        </div>
        <hr>
        <div id="proyectos-container">

        </div>
                <!--
            <div class="row">
                <div class="col-sm-12 d-none d-md-block col-md-2">
                    <img id="Imagen"src="../ImagenUsuario.png" alt="">
                </div>
                <div class="col-sm-12 col-md-8">
                    <b>Nombre del Proyecto</b>
                    <div class="row">
                        <div class="col-12">
                            Fecha de Entrega: 25/09/2020
                        </div>
                        <div class="col-12 pt-3">
                            <b>Descripción del proyecto</b>
                        </div>
                        <div class="col-12">
                            Se requiere un Software que nos permita acceder a cualquier plataforma sin iniciar Sesion
                        </div>
                        <div class="col-12 pt-3">
                            <b>Restricciones del proyecto</b>
                        </div>
                        <div class="col-12">
                            No se puede hacer uso de ningun tipo de librerias, el trabajo solo puede ser desarrollado por una persona y deberá tener 20 años de experiencia
                        </div>
                        <div class="col-12 pt-3">
                            <b>Observaciones Técincas</b>
                        </div>
                        <div class="col-12">
                            Deberá ser desarrollado en c#
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="row">
                        <div class="col-12">
                            <b>Presupuesto</b>
                        </div>
                        <div class="col-12">
                            $400,000,000.00
                        </div>
                        <div class="col-12 py-2">
                            <button id="VerMas">Ver más Informacion</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12 d-none d-md-block col-md-2">
                    <img id="Imagen" src="../ImagenUsuario.png" alt="">
                </div>
                <div class="col-sm-12 col-md-8">
                    <b>Nombre del Proyecto</b>
                    <div class="row">
                        <div class="col-12">
                            Fecha de Entrega: 25/09/2020
                        </div>
                        <div class="col-12 pt-3">
                            <b>Descripción del proyecto</b>
                        </div>
                        <div class="col-12">
                            Se requiere un Software que nos permita acceder a cualquier plataforma sin iniciar Sesion
                        </div>
                        <div class="col-12 pt-3">
                            <b>Restricciones del proyecto</b>
                        </div>
                        <div class="col-12">
                            No se puede hacer uso de ningun tipo de librerias, el trabajo solo puede ser desarrollado por una
                            persona y deberá tener 20 años de experiencia
                        </div>
                        <div class="col-12 pt-3">
                            <b>Observaciones Técincas</b>
                        </div>
                        <div class="col-12">
                            Deberá ser desarrollado en c#
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="row">
                        <div class="col-12">
                            <b>Presupuesto</b>
                        </div>
                        <div class="col-12">
                            $400,000,000.00
                        </div>
                        <div class="col-12 py-2">
                            <button id="VerMas">Ver más Informacion</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12 d-none d-md-block col-md-2">
                    <img id="Imagen" src="../ImagenUsuario.png" alt="">
                </div>
                <div class="col-sm-12 col-md-8">
                    <b>Nombre del Proyecto</b>
                    <div class="row">
                        <div class="col-12">
                            Fecha de Entrega: 25/09/2020
                        </div>
                        <div class="col-12 pt-3">
                            <b>Descripción del proyecto</b>
                        </div>
                        <div class="col-12">
                            Se requiere un Software que nos permita acceder a cualquier plataforma sin iniciar Sesion
                        </div>
                        <div class="col-12 pt-3">
                            <b>Restricciones del proyecto</b>
                        </div>
                        <div class="col-12">
                            No se puede hacer uso de ningun tipo de librerias, el trabajo solo puede ser desarrollado por una
                            persona y deberá tener 20 años de experiencia
                        </div>
                        <div class="col-12 pt-3">
                            <b>Observaciones Técincas</b>
                        </div>
                        <div class="col-12">
                            Deberá ser desarrollado en c#
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="row">
                        <div class="col-12">
                            <b>Presupuesto</b>
                        </div>
                        <div class="col-12">
                            $400,000,000.00
                        </div>
                        <div class="col-12 py-2">
                            <button id="VerMas">Ver más Informacion</button>
                        </div>
                    </div>
                </div>
            </div>
        -->
        
    </div>
    <hr>
    <script src="../js/funciones_principal.js"></script>
    <script src="../js/peticiones_principal.js"></script>
    
</body>

</html>