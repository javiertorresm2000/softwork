<?php
    class Proyecto{
        private $db;
		private $result;

		public function __construct() {
			require_once '../config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
        }
        
        public function insert_nuevo_proyecto($data){
            try{
                $titulo = $data['titulo'];
                $descripcion_general = $data['descripcion_general'];
                $restricciones = $data['restricciones'];
                $observaciones_tecnicas = $data['observaciones'];
                $fecha_entrega_tentativa = $data['fecha_entrega'];
                $presupuesto_minimo = $data['presupuesto_min'];
                $presupuesto_maximo = $data['presupuesto_max'];
                $usuario_propietario = $data['id_usuario'];

                $sql = "INSERT INTO 
                            proyecto
                        VALUES (
                            null,
                            :titulo,
                            :descripcion_general,
                            :restricciones,
                            :observaciones_tecnicas,
                            NOW(),
                            NOW(),
                            :fecha_entrega_tentativa,
                            null,
                            :presupuesto_minimo,
                            :presupuesto_maximo,
                            null, 
                            :usuario_propietario,
                            null,
                            null,
                            1)";
                
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":titulo",$titulo, PDO::PARAM_STR);
                $sql->bindParam(":descripcion_general",$descripcion_general, PDO::PARAM_STR);
                $sql->bindParam(":restricciones",$restricciones, PDO::PARAM_STR);
                $sql->bindParam(":observaciones_tecnicas",$observaciones_tecnicas, PDO::PARAM_STR);
                $sql->bindParam(":fecha_entrega_tentativa",$fecha_entrega_tentativa, PDO::PARAM_STR);
                $sql->bindParam(":presupuesto_minimo",$presupuesto_minimo, PDO::PARAM_STR);
                $sql->bindParam(":presupuesto_maximo",$presupuesto_maximo, PDO::PARAM_STR);
                $sql->bindParam(":usuario_propietario",$usuario_propietario, PDO::PARAM_INT);
                $sql->execute();
				$this->result = $this->db->lastInsertId();
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function update_proyecto($data){
            try{
                $this->db->beginTransaction();

                $titulo = $data['titulo'];
                $descripcion_general = $data['descripcion_general'];
                $restricciones = $data['restricciones'];
                $observaciones_tecnicas = $data['observaciones_tecnicas'];
                $fecha_entrega_tentativa = $data['fecha_entrega_tentativa'];
                $presupuesto_minimo = $data['presupuesto_minimo'];
                $presupuesto_maximo = $data['presupuesto_maximo'];
                $id_proyecto = $data['is_proyecto'];

                $sql = "UPDATE
                            proyecto
                        SET
                            titulo = :titulo,
                            descripcion_general = :descripcion_general,
                            restricciones = :restricciones,
                            observaciones_tecnicas = :observaciones_tecnicas,
                            fecha_ultima_actualizacion = NOW(),
                            fecha_entrega_tentativa = :fecha_entrega_tentativa,
                            presupuesto_minimo = :presupuesto_minimo,
                            presupuesto_maximo = :presupuesto_maximo
                        WHERE
                            id_proyecto = :id_proyecto";
                
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_proyecto",$id_proyecto, PDO::PARAM_INT);
                $sql->bindParam(":titulo",$titulo, PDO::PARAM_STR);
                $sql->bindParam(":descripcion_general",$descripcion_general, PDO::PARAM_STR);
                $sql->bindParam(":restricciones",$restricciones, PDO::PARAM_STR);
                $sql->bindParam(":observaciones_tecnicas",$observaciones_tecnicas, PDO::PARAM_STR);
                $sql->bindParam(":fecha_entrega_tentativa",$fecha_entrega_tentativa, PDO::PARAM_STR);
                $sql->bindParam(":presupuesto_minimo",$presupuesto_minimo, PDO::PARAM_STR);
                $sql->bindParam(":presupuesto_maximo",$presupuesto_maximo, PDO::PARAM_STR);

				$this->result = "Actualizado con exito";
				$this->db->commit();
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_proyectos(){
            try{
                $sql = "SELECT 
                            *
                        FROM 
                            proyecto 
                        WHERE status = 1";
                $sql = $this->db->prepare($sql);
                $sql->execute();
                $this->result = $sql->fetchAll(PDO::FETCH_ASSOC);

            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_todos_mis_proyectos($id_usuario){
            try{
                //$id_usuario = $data['id_usuario'];

                $sql = "SELECT 
                            id_proyecto,
                            titulo,
                            descripcion_general,
                            fecha_ultima_actualizacion,
                            status
                        FROM 
                            proyecto 
                        WHERE 
                            usuario_propietario = :id_usuario
                        AND
                            status != 5";
                            
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario",$id_usuario, PDO::PARAM_INT);
                $sql->execute();
                $this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
                
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_mis_proyectos_activos($id_usuario){
            try{
                //$id_usuario = $data['id_usuario'];

                $sql = "SELECT 
                            id_proyecto,
                            titulo,
                            descripcion_general,
                            fecha_ultima_actualizacion
                        FROM 
                            proyecto 
                        WHERE 
                            status = 1
                        AND
                            usuario_propietario = :id_usuario";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario",$id_usuario, PDO::PARAM_INT);
                $sql->execute();
                $this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
                
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_mis_proyectos_en_proceso($id_usuario){
            try{
                //$id_usuario = $data['id_usuario'];

                $sql = "SELECT 
                            id_proyecto,
                            titulo,
                            descripcion_general,
                            fecha_ultima_actualizacion
                        FROM 
                            proyecto 
                        WHERE 
                            status = 3
                        AND
                            usuario_propietario = :id_usuario";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario",$id_usuario, PDO::PARAM_INT);
                $sql->execute();
                $this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
                
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_mis_proyectos_inactivos($id_usuario){
            try{
                //$id_usuario = $data['id_usuario'];

                $sql = "SELECT 
                            id_proyecto,
                            titulo,
                            descripcion_general,
                            fecha_ultima_actualizacion
                        FROM 
                            proyecto 
                        WHERE 
                            status = 2
                        AND
                            usuario_propietario = :id_usuario";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario",$id_usuario, PDO::PARAM_INT);
                $sql->execute();
                $this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
                
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_mis_proyectos_finalizados($id_usuario){
            try{
                //$id_usuario = $data['id_usuario'];

                $sql = "SELECT 
                            id_proyecto,
                            titulo,
                            descripcion_general,
                            fecha_ultima_actualizacion
                        FROM 
                            proyecto 
                        WHERE 
                            status = 4
                        AND
                            usuario_propietario = :id_usuario";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario",$id_usuario, PDO::PARAM_INT);
                $sql->execute();
                $this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
                
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_proyecto_especifico_profile($id_proyecto){ 
            try{
                //$id_proyecto = $data['id_proyecto'];

                $sql = "SELECT * FROM proyecto WHERE id_proyecto = :id_proyecto ";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_proyecto",$id_proyecto, PDO::PARAM_INT);
                $sql->execute();
                $this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
                
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_proyecto_especifico_explore($id_proyecto){ 
            try{
                //$id_proyecto = $data['id_proyecto'];

                $sql = "SELECT 
                            *,
                            (SELECT CONCAT(nombre, ' ', apellidos) AS nombre_usuario FROM usuarios WHERE id_usuario = usuario_propietario LIMIT 1) AS nombre_usuario,
                            (SELECT descripcion_personal FROM usuarios WHERE id_usuario = usuario_propietario LIMIT 1)as descripcion_personal,
                            (SELECT COUNT(proyecto) FROM propuesta WHERE proyecto = id_proyecto) as num_propuestas
                        FROM proyecto WHERE id_proyecto = :id_proyecto ";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_proyecto",$id_proyecto, PDO::PARAM_INT);
                $sql->execute();
                $this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
                
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function update_deshabilitar_proyecto($id_proyecto){
            try{

                $sql = "UPDATE proyecto SET status = 2 WHERE id_proyecto = :id_proyecto";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_proyecto",	$id_proyecto, PDO::PARAM_INT);
                $sql->execute();
                $this->result = "Actualizado";
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function update_habilitar_proyecto($id_proyecto){
            try{
                $sql = "UPDATE proyecto SET status = 1 WHERE id_proyecto = :id_proyecto";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_proyecto",	$id_proyecto, PDO::PARAM_INT);
                $sql->execute();
                $this->result = "Actualizado";
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function update_finalizar_proyecto($id_proyecto){
            try{
                $sql = "UPDATE proyecto SET status = 4 WHERE id_proyecto = :id_proyecto";
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_proyecto",	$id_proyecto, PDO::PARAM_INT);
                $sql->execute();

                $sql = "SELECT propuesta_aceptada FROM proyecto WHERE id_proyecto = :id_proyecto";
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_proyecto",	$id_proyecto, PDO::PARAM_INT);
                $sql->execute();

                $result = $sql->fetch(PDO::FETCH_ASSOC);

                $sql = "UPDATE propuesta SET status = 5 WHERE id_propuesta = :id_propuesta";
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_propuesta",	$result['propuesta_aceptada'], PDO::PARAM_INT);
                $sql->execute();


                $this->result = "Actualizado";
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function delete_proyecto($data){
            try{
                $id_proyecto = $data['id_proyecto'];

                $sql = "UPDATE proyecto SET status = 5 WHERE id_proyecto = :id_proyecto";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_proyecto",	$id_proyecto, PDO::PARAM_INT);
                $sql->execute();
                $this->result = "Eliminado con exito";
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

    }

?>