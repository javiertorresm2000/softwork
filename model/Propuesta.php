<?php
    class Propuesta{
        private $db;
		private $result;

		public function __construct() {
			require_once '../config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
		}

		public function insert_nueva_propuesta($data){
			try{

                $descripcion_general 	= $data['descripcion_propuesta'];
                $especificacion_tecnica = $data['especificaciones_propuesta'];
                $fecha_entrega 			= $data['fecha_entrega_propuesta'];
                $cotizacion 			= $data['presupuesto_propuesta'];
                $usuario_propietario 	= $data['id_usuario'];
                $proyecto 				= $data['id_proyecto'];

                $sql = "INSERT INTO 
                            propuesta
                        VALUES (
                            null,
                            :descripcion_general,
                            :especificacion_tecnica,
                            NOW(),
                            :fecha_entrega,
                            :cotizacion,
                            :usuario_propietario,
                            :proyecto,
                            1)";
                
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":descripcion_general",$descripcion_general, PDO::PARAM_STR);
                $sql->bindParam(":especificacion_tecnica",$especificacion_tecnica, PDO::PARAM_STR);
                $sql->bindParam(":fecha_entrega",$fecha_entrega, PDO::PARAM_STR);
                $sql->bindParam(":cotizacion",$cotizacion, PDO::PARAM_STR);
                $sql->bindParam(":proyecto",$proyecto, PDO::PARAM_STR);
                $sql->bindParam(":usuario_propietario",$usuario_propietario, PDO::PARAM_INT);

				$sql->execute();
				$this->result = $this->db->lastInsertId();
				

            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		public function select_propuestas_proyecto($id_proyecto){
			try{
				//$id_proyecto = $data['data'];

				$sql = "SELECT 
							p.id_propuesta,
							p.descripcion_general,
							p.fecha_creacion,
							p.cotizacion,
							p.status,
							(SELECT CONCAT(u.nombre, ' ',u.apellidos) AS nombre_propietario FROM usuarios u WHERE u.id_usuario = p.usuario_propietario) AS nombre_propietario,
							(SELECT u.propuestas_aceptadas FROM usuarios u WHERE u.id_usuario = p.usuario_propietario LIMIT 1) AS num_participaciones
						FROM
							propuesta p
						WHERE 
							p.proyecto = :id_proyecto
						ORDER BY 
							p.fecha_creacion DESC";

				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_proyecto",$id_proyecto, PDO::PARAM_INT);
				$sql->execute();
				$propuestas = $sql->fetchAll(PDO::FETCH_ASSOC);

				$sql = "SELECT propuesta_aceptada FROM proyecto WHERE id_proyecto = :id_proyecto";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_proyecto",$id_proyecto, PDO::PARAM_INT);
				$sql->execute();
				$res =  $sql->fetch(PDO::FETCH_ASSOC);

				if($res['propuesta_aceptada'] != null && $res['propuesta_aceptada'] != ""){

					$propuesta_aceptada = $res['propuesta_aceptada'];
					$propuesta = $this->select_propuesta_especifica($propuesta_aceptada);
				}
				else{
					$propuesta = "";
				}

				$result = array(
					"propuesta" => $propuesta,
					"propuestas" => $propuestas,
				);

				$this->result = $result;
			}catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		public function select_mis_propuestas($id_usuario){
			try{

				$sql = "SELECT 
							p.id_propuesta,
							p.fecha_creacion,
							p.fecha_entrega,
							p.cotizacion,
							p.proyecto,
							(SELECT titulo FROM proyecto WHERE id_proyecto = p.proyecto LIMIT 1) AS titulo_proyecto,
							p.status
						FROM
							propuesta p
						WHERE 
							p.usuario_propietario = :id_usuario";

				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_usuario",$id_usuario, PDO::PARAM_INT);
				$sql->execute();
				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
			}catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		public function select_propuesta_especifica($id_propuesta){
			try{
				$sql = "SELECT 
							*,
							(SELECT CONCAT(u.nombre, ' ',u.apellidos) AS nombre_propietario FROM usuarios u WHERE u.id_usuario = usuario_propietario) AS nombre_propietario,
							(SELECT u.carrera FROM usuarios u WHERE u.id_usuario = usuario_propietario LIMIT 1) AS especialidad,
							(SELECT u.propuestas_aceptadas FROM usuarios u WHERE u.id_usuario = usuario_propietario LIMIT 1) AS num_participaciones
						FROM 
							propuesta 
						WHERE 
							id_propuesta = :id_propuesta";

				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_propuesta",$id_propuesta, PDO::PARAM_INT);
				$sql->execute();
				$this->result = $sql->fetch(PDO::FETCH_ASSOC);
			}catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		public function update_aceptar_propuesta($id_propuesta){
			try{
				// ACTUALIZAR EL ESTADO DE LA PROPUESTA
				$sql = "UPDATE propuesta SET status = 4 WHERE id_propuesta = :id_propuesta";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_propuesta",$id_propuesta, PDO::PARAM_INT);
				$sql->execute();


				$sql = "SELECT * FROM propuesta WHERE id_propuesta = :id_propuesta";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_propuesta",$id_propuesta, PDO::PARAM_INT);
				$sql->execute();
				$data = $sql->fetch(PDO::FETCH_ASSOC);

				// ACTUALIZAR EL PROYECTO
				$id_proyecto = $data['proyecto'];
				$presupuesto_aceptado = $data['cotizacion'];
				$usuario_aceptado = $data['usuario_propietario'];
				$fecha_entrega = $data['fecha_entrega'];

				$sql = "UPDATE 
							proyecto 
						SET 
							presupuesto_aceptado = :presupuesto_aceptado,
							usuario_aceptado = :usuario_aceptado,
							propuesta_aceptada = :id_propuesta,
							fecha_entrega_aceptada = :fecha_entrega,
							status = 3 
						WHERE 
							id_proyecto = :id_proyecto";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_propuesta",$id_propuesta, PDO::PARAM_INT);
				$sql->bindParam(":presupuesto_aceptado",$presupuesto_aceptado, PDO::PARAM_STR);
				$sql->bindParam(":usuario_aceptado",$usuario_aceptado, PDO::PARAM_INT);
				$sql->bindParam(":fecha_entrega",$fecha_entrega, PDO::PARAM_STR);
				$sql->bindParam(":id_proyecto",$id_proyecto, PDO::PARAM_INT);
				$sql->execute();

				// ACTUALIZAR LAS PROPUESTAS SOBRANTES 
				$sql = "UPDATE 
							propuesta
						SET 
							status = 3 
						WHERE 
							proyecto = :id_proyecto
						AND 
							id_propuesta != :id_propuesta";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_propuesta",$id_propuesta, PDO::PARAM_INT);
				$sql->bindParam(":id_proyecto",$id_proyecto, PDO::PARAM_INT);
				$sql->execute();
				
				$result = array(
					"proyecto" => "",
					"propuesta" => ""
				);
				$result['proyecto'] = $id_proyecto;
				$result['propuesta'] = $id_propuesta;

				$this->result = $result;
			}catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		public function update_propuesta_revisada($id_propuesta){
			try{
				// ACTUALIZAR EL ESTADO DE LA PROPUESTA A REVISADO
				$sql = "UPDATE propuesta SET status = 2 WHERE id_propuesta = :id_propuesta";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_propuesta",$id_propuesta, PDO::PARAM_INT);
				$sql->execute();

				$sql = "SELECT proyecto FROM propuesta WHERE id_propuesta = :id_propuesta";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_propuesta",$id_propuesta, PDO::PARAM_INT);
				$sql->execute();

				$this->result = $sql->fetch(PDO::FETCH_ASSOC);;
			}catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		public function update_rechazar_propuesta($id_propuesta){
			try{
				// ACTUALIZAR EL ESTADO DE LA PROPUESTA
				$sql = "UPDATE propuesta SET status = 3 WHERE id_propuesta = :id_propuesta";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_propuesta",$id_propuesta, PDO::PARAM_INT);
				$sql->execute();

				$this->result = "Actualizado";
			}catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		public function update_habilitar_propuesta_rechazada($id_propuesta){
			try{
				// ACTUALIZAR EL ESTADO DE LA PROPUESTA
				$sql = "UPDATE propuesta SET status = 2 WHERE id_propuesta = :id_propuesta";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_propuesta",$id_propuesta, PDO::PARAM_INT);
				$sql->execute();

				$this->result = "Actualizado";
			}catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

    }

?>