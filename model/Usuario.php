<?php
    class Usuario{
        private $db;
        private $result;
        
        private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
        );

		public function __construct() {
			require_once '../config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
        }
        
        public function insert_nuevo_usuario($data){
            try{
                $nombre                     = $data['nombre'];
                $apellidos                  = $data['apellidos'];
                $correo                     = $data['correo'];
                $contrasenia                = $data['contrasenia'];
                $pregunta_seguridad         = $data['pregunta_seguridad'];
                $respuesta_seguridad        = $data['respuesta_seguridad'];
                $telefono                   = $data['telefono'];
                $fecha_nacimiento            = $data['fecha_nacimiento'];
                $direccion                  = $data['direccion'];
                $municipio                  = $data['municipio'];
                $estado                     = $data['estado'];
                $foto                       = "foto.jpg";
                $descripcion_personal       = $data['descripcion_personal'];
                $grado_academico            = $data['grado_academico'];
                $carrera                    = $data['carrera'];
                $experiencia_profesional    = $data['experiencia_profesional'];


                $sql = "INSERT INTO 
                            usuarios
                        VALUES(
                            null,
                            :nombre,
                            :apellidos,
                            :correo,
                            :contrasenia,
                            :pregunta_seguridad,
                            :respuesta_seguridad,
                            :telefono,
                            null,
                            :fecha_nacimiento,
                            :direccion,
                            :municipio,
                            :estado,
                            null,
                            :descripcion_personal,
                            :grado_academico,
                            :carrera,
                            :experiencia_profesional,
                            0,
                            0,
                            NOW(),
                            0,
                            1
                            )";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":nombre",$nombre, PDO::PARAM_STR);
                $sql->bindParam(":apellidos",$apellidos, PDO::PARAM_STR);
                $sql->bindParam(":correo",$correo, PDO::PARAM_STR);
                $sql->bindParam(":contrasenia",$contrasenia, PDO::PARAM_STR);
                $sql->bindParam(":pregunta_seguridad",$pregunta_seguridad, PDO::PARAM_INT);
                $sql->bindParam(":respuesta_seguridad",$respuesta_seguridad, PDO::PARAM_STR);
                $sql->bindParam(":fecha_nacimiento",$fecha_nacimiento, PDO::PARAM_STR);
                $sql->bindParam(":telefono",$telefono, PDO::PARAM_STR);
                $sql->bindParam(":direccion",$direccion, PDO::PARAM_STR);
                $sql->bindParam(":municipio",$municipio, PDO::PARAM_INT);
                $sql->bindParam(":estado",$estado, PDO::PARAM_INT);
                $sql->bindParam(":descripcion_personal",$descripcion_personal, PDO::PARAM_STR);
                $sql->bindParam(":grado_academico",$grado_academico, PDO::PARAM_INT);
                $sql->bindParam(":carrera",$carrera, PDO::PARAM_STR);
                $sql->bindParam(":experiencia_profesional",$experiencia_profesional, PDO::PARAM_STR);
                $sql->execute();
				$this->result = $this->db->lastInsertId();

				
                
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function update_usuario($data){
            try{
                $id_usuario                 = $data['id_usuario'];
                $nombre                     = $data['nombre'];
                $apellidos                  = $data['apellidos'];
                $telefono                   = $data['telefono'];
                $fecha_nacimiento           = $data['fecha_nacimiento'];
                $genero                     = $data['genero'];
                $direccion                  = $data['direccion'];
                $municipio                  = $data['municipio'];
                $estado                     = $data['estado'];
                $descripcion_personal       = $data['descripcion_personal'];
                $grado_academico            = $data['grado_academico'];
                $carrera                    = $data['carrera'];
                $experiencia_profesional    = $data['experiencia_profesional'];


                $sql = "UPDATE
                            usuarios
                        SET
                            nombre = :nombre,
                            apellidos = :apellidos,
                            telefono = :telefono,
                            genero = :genero,
                            fecha_nacimiento = :fecha_nacimiento,
                            direccion = :direccion,
                            municipio = :municipio,
                            estado = :estado,
                            descripcion_personal = :descripcion_personal,
                            grado_academico = :grado_academico,
                            carrera = :carrera,
                            experiencia_profesional = :experiencia_profesional
                        WHERE
                            id_usuario = :id_usuario";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario",$id_usuario, PDO::PARAM_INT);
                $sql->bindParam(":nombre",$nombre, PDO::PARAM_STR);
                $sql->bindParam(":apellidos",$apellidos, PDO::PARAM_STR);
                $sql->bindParam(":fecha_nacimiento",$fecha_nacimiento, PDO::PARAM_STR);
                $sql->bindParam(":telefono",$telefono, PDO::PARAM_STR);
                $sql->bindParam(":genero",$genero, PDO::PARAM_STR);
                $sql->bindParam(":direccion",$direccion, PDO::PARAM_STR);
                $sql->bindParam(":municipio",$municipio, PDO::PARAM_INT);
                $sql->bindParam(":estado",$estado, PDO::PARAM_INT);
                $sql->bindParam(":descripcion_personal",$descripcion_personal, PDO::PARAM_STR);
                $sql->bindParam(":grado_academico",$grado_academico, PDO::PARAM_INT);
                $sql->bindParam(":carrera",$carrera, PDO::PARAM_STR);
                $sql->bindParam(":experiencia_profesional",$experiencia_profesional, PDO::PARAM_STR);
                $sql->execute();
				$this->result = "Exito";

            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function validar_correo_no_registrado($data){
            try{
                $correo = $data['correo'];

                $sql = "SELECT nombre, id_usuario FROM usuarios WHERE correo = :correo LIMIT 1";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":correo",	$correo, PDO::PARAM_STR);
                $sql->execute();
                $resultado = $sql->fetch(PDO::FETCH_ASSOC);	
                if($resultado){
                    $this->result = "1";
                }else{
                    $this->result = "0";
                }
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function login_usuario($data){
            try{
                $correo = $data['correo'];

                $sql = "SELECT id_usuario FROM usuarios WHERE correo = :correo LIMIT 1";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":correo",	$correo, PDO::PARAM_STR);
                $sql->execute();
                $resultado = $sql->fetch(PDO::FETCH_ASSOC);	

                if($resultado){
                    $id_usuario = $resultado['id_usuario'];

                    $sql = "SELECT contrasenia, intentos_fallidos, status FROM usuarios WHERE id_usuario = :id_usuario";

                    $sql = $this->db->prepare($sql);
                    $sql->bindParam(":id_usuario",	$id_usuario, PDO::PARAM_INT);
                    $sql->execute();

                    $resultado = $sql->fetch(PDO::FETCH_ASSOC);

                    // VALIDAR SI ESTA ACTIVO EL USUARIO
                    if($resultado['status']==1){
                        // VALIDAR SI SU CONTRASEÑA ES CORRECTA
                        if($resultado['contrasenia'] == $data['contrasenia']){
                            $sql = "SELECT * FROM usuarios WHERE id_usuario = :id_usuario";
    
                            $sql = $this->db->prepare($sql);
                            $sql->bindParam(":id_usuario",	$id_usuario, PDO::PARAM_INT);
                            $sql->execute();
                            
                            $this->response['status'] = "Loggeado";
                            $this->response['body'] = "1";
                            $this->response['data'] = $sql->fetch(PDO::FETCH_ASSOC);
                            $res = $this->response['data'];

                            session_start();
                            
                            $_SESSION['logged'] = 1;
                            $_SESSION['id'] =  $res['id_usuario'];
                            $_SESSION['nombre'] = "".$res['nombre']." ".$res['apellidos'];
                            $_SESSION['usuario'] = $res['correo'];

                            // ACTUALIZA LOS INTENTOS DEL USUARIO A 0
                            $sql = "UPDATE usuarios SET intentos_fallidos = 0 WHERE id_usuario = :id_usuario";
    
                            $sql = $this->db->prepare($sql);
                            $sql->bindParam(":id_usuario",	$id_usuario, PDO::PARAM_INT);
                            $sql->execute();

                            // REGISTRA EL INICIO DE SESION
                            $sql = "INSERT INTO detalle_login 
                                    VALUES(
                                        null,
                                        NOW(),
                                        null,
                                        :id_usuario)";
    
                            $sql = $this->db->prepare($sql);
                            $sql->bindParam(":id_usuario",	$id_usuario, PDO::PARAM_INT);
                            $sql->execute();
                            $_SESSION['sesion'] = $this->db->lastInsertId();
                        }
                        // SI SU CONTRASEÑA NO ES CORRECTA
                        else{
                            // VALIDAR SI TIENE 3 INTENTOS PPARA PROCEDER A BLOQUEO DE CUENTA
                            if($resultado['intentos_fallidos']==3){
                                $resultado['intentos_fallidos']=4;
                                $sql = "UPDATE 
                                            usuarios 
                                        SET 
                                            intentos_fallidos = 4,
                                            status = 2
                                        WHERE id_usuario = :id_usuario";
    
                                $sql = $this->db->prepare($sql);
                                $sql->bindParam(":id_usuario",	$id_usuario, PDO::PARAM_INT);
                                $sql->execute();

                                $this->response['status'] = "Limite de intentos";
                                $this->response['body'] = "4";
                                $this->response['data'] = $resultado;
                                
                                //$this->result = $resultado['intentos_fallidos'];
                            }
                            // SI NO TIENE 3 INTENTOS SOLO SE SUMA UNO MAS A SUS INTENTOS FALLIDOS
                            else{
                                $sql = "UPDATE usuarios SET intentos_fallidos = intentos_fallidos+1 WHERE id_usuario = :id_usuario";
    
                                $sql = $this->db->prepare($sql);
                                $sql->bindParam(":id_usuario",	$id_usuario, PDO::PARAM_INT);
                                $sql->execute();
                                $resultado['intentos_fallidos'] = $resultado['intentos_fallidos']+1;

                                $this->response['status'] = "Contraseña incorrecta";
                                $this->response['body'] = "3";
                                $this->response['data'] = $resultado;
                                //$this->result = $resultado['intentos_fallidos'];
                            }
                        }
                    // ENTONCES EL USUARIO ESTA BLOQUEADO O INACTIVO
                    }else{
                        // REGRESAMOS EL STATUS DEL USUARIO
                        $this->response['status'] = "Usuario bloqueado";
                        $this->response['body'] = "4";
                        $this->response['data'] = $resultado;
                        //$this->result = $resultado['status'];
                    }
                }else{
                    $this->response['status'] = "No Registrado";
                    $this->response['body'] = "2";
                    $this->response['data'] = $resultado;
                }
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->response;
        }

        public function actualizar_tiempo_sesion($data){
            try{
                $sesion = $data['sesion'];
                $minutos = $data['minutos'];

                $sql = "UPDATE 
                            detalle_login
                        SET 
                            tiempo_login = :minutos
                        WHERE 
                            id_detalle_login = :sesion";
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":minutos",	$minutos, PDO::PARAM_STR);
                $sql->bindParam(":sesion",	$sesion, PDO::PARAM_STR);
                $sql->execute();

                $this->result = $minutos;

            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_pregunta_seguridad($correo){
            try{

                $sql = "SELECT 
                            u.id_usuario,
                            (SELECT pregunta FROM pregunta_seguridad WHERE id_pregunta = u.pregunta_seguridad LIMIT 1) AS pregunta
                        FROM 
                            usuarios u
                        WHERE 
                            u.correo = :correo";
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":correo",	$correo, PDO::PARAM_STR);
                $sql->execute();
                $this->result = $sql->fetch(PDO::FETCH_ASSOC);

            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function validar_respuesta_seguridad($data){
            try{
                $id_usuario = $data['id_usuario'];
                $respuesta = $data['respuesta'];
                $sql = "SELECT 
                            u.respuesta_seguridad
                        FROM 
                            usuarios u
                        WHERE 
                            u.id_usuario = :id_usuario";
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario",	$id_usuario, PDO::PARAM_INT);
                $sql->execute();
                $res = $sql->fetch(PDO::FETCH_ASSOC);

                if($res['respuesta_seguridad'] == $respuesta){
                    $this->result = "Correcto";
                }
                else{
                    $this->result = "Incorrecto";
                }
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function update_contrasenia($data){
            try{
                $contrasenia_nueva = $data['contrasenia'];
                $id_usuario = $data['id_usuario'];

                $sql = "UPDATE usuarios SET contrasenia = :contrasenia_nueva WHERE id_usuario = :id_usuario";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario", $id_usuario, PDO::PARAM_INT);
                $sql->bindParam(":contrasenia_nueva", $contrasenia_nueva, PDO::PARAM_STR);
                $sql->execute();

                $sql = "UPDATE usuarios SET intentos_fallidos = 0 WHERE id_usuario = :id_usuario";
    
                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario",	$id_usuario, PDO::PARAM_INT);
                $sql->execute();

                $this->result = "Actualizado";
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }

        public function select_usuario($id_usuario){
            try{
                $sql = "SELECT 
                            nombre,
                            apellidos,
                            correo,
                            telefono,
                            genero,
                            fecha_nacimiento,
                            direccion,
                            municipio,
                            estado,
                            foto,
                            descripcion_personal,
                            grado_academico,
                            carrera,
                            experiencia_profesional,
                            propuestas_aceptadas,
                            proyectos_aceptados
                        FROM
                            usuarios
                        WHERE
                            id_usuario = :id_usuario
                        ";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario", $id_usuario, PDO::PARAM_INT);
                $sql->execute();
                $this->result = $sql->fetch(PDO::FETCH_ASSOC);
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }


        public function select_desempenio_usuario($id_usuario){
            try{
                $sql = "SELECT
                            nombre, 
                            (SELECT COUNT(id_propuesta) FROM propuesta WHERE (status = 1 OR status = 2) AND usuario_propietario = :id_usuario) AS prop_activas,
                            (SELECT COUNT(id_propuesta) FROM propuesta WHERE usuario_propietario = :id_usuario AND status = 4)AS prop_aceptadas,
                            (SELECT COUNT(id_propuesta) FROM propuesta WHERE usuario_propietario = :id_usuario AND status = 5)AS prop_finalizadas,
                            (SELECT COUNT(id_propuesta) FROM propuesta WHERE usuario_propietario = :id_usuario AND status = 3)AS prop_rechazadas,
                            (SELECT COUNT(id_proyecto) FROM proyecto WHERE usuario_propietario = :id_usuario AND status = 1)AS proy_activos,
                            (SELECT COUNT(id_proyecto) FROM proyecto WHERE usuario_propietario = :id_usuario AND status = 2)AS proy_inactivos,
                            (SELECT COUNT(id_proyecto) FROM proyecto WHERE usuario_propietario = :id_usuario AND status = 3)AS proy_proceso,
                            (SELECT COUNT(id_proyecto) FROM proyecto WHERE usuario_propietario = :id_usuario AND status = 4)AS proy_finalizados
                        FROM
                            usuarios
                        WHERE
                            id_usuario = :id_usuario
                        ";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id_usuario", $id_usuario, PDO::PARAM_INT);
                $sql->execute();
                $this->result = $sql->fetch(PDO::FETCH_ASSOC);
            }catch (PDOException $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
        }
    }

?>