<?php
	switch($_GET['controller']){
		case 'listar_proyectos':{
			require_once '../controller/proyecto_controller.php';
			$controller = new Proyectos();
			$controller->listar_proyectos();
			break;
		}
		case 'listar_mis_proyectos_activos':{
			require_once '../controller/proyecto_controller.php';
			$controller = new Proyectos();
			$controller->listar_mis_proyectos($_POST['id_usuario']);
			break;
        }
        case 'guardar_nuevo_proyecto':{
			require_once '../controller/proyecto_controller.php';
			$controller = new Proyectos();
			$controller->nuevo_proyecto($_POST['data']);
			break;
		}
		case 'detalle_proyecto':{
			require_once '../controller/proyecto_controller.php';
			$controller = new Proyectos();
			$controller->detalle_proyecto($_POST['id']);
			break;
		}
		
		case 'deshabilitar_proyecto':{
			require_once '../controller/proyecto_controller.php';
			$controller = new Proyectos();
			$controller->deshabilitar_proyecto($_POST['id']);
			break;
		}
		
		case 'habilitar_proyecto':{
			require_once '../controller/proyecto_controller.php';
			$controller = new Proyectos();
			$controller->habilitar_proyecto($_POST['id']);
			break;
		}
		
		case 'finalizar_proyecto':{
			require_once '../controller/proyecto_controller.php';
			$controller = new Proyectos();
			$controller->finalizar_proyecto($_POST['id']);
			break;
        }
	}
?>