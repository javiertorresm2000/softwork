<?php
	switch($_GET['controller']){
        case 'guardar_propuesta':{
			require_once '../controller/propuesta_controller.php';
			$controller = new Propuestas();
			$controller->nueva_propuesta($_POST['data']);
			break;
		}

		case 'listar_propuestas':{
			require_once '../controller/propuesta_controller.php';
			$controller = new Propuestas();
			$controller->listar_propuestas($_POST['id']);
			break;
		}

		case 'listar_mis_propuestas':{
			require_once '../controller/propuesta_controller.php';
			$controller = new Propuestas();
			$controller->listar_mis_propuestas($_POST['id_usuario']);
			break;
		}

		case 'detalle_propuesta':{
			require_once '../controller/propuesta_controller.php';
			$controller = new Propuestas();
			$controller->detalle_propuesta($_POST['id']);
			break;
		}

		case 'propuesta_revisada':{
			require_once '../controller/propuesta_controller.php';
			$controller = new Propuestas();
			$controller->propuesta_revisada($_POST['id']);
			break;
		}

		case 'aceptar_propuesta':{
			require_once '../controller/propuesta_controller.php';
			$controller = new Propuestas();
			$controller->aceptar_propuesta($_POST['id']);
			break;
		}

		case 'rechazar_propuesta':{
			require_once '../controller/propuesta_controller.php';
			$controller = new Propuestas();
			$controller->rechazar_propuesta($_POST['id']);
			break;
		}

		case 'habilitar_propuesta_rechazada':{
			require_once '../controller/propuesta_controller.php';
			$controller = new Propuestas();
			$controller->habilitar_propuesta_rechazada($_POST['id']);
			break;
		}
	}
?>