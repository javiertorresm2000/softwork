<?php
	switch($_GET['controller']){
		case 'login':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->login($_POST['data']);
			break;
        }
        case 'validar_correo':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->validar_correo($_POST['data']);
			break;
        }
        case 'registrar_nuevo_usuario':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->nuevo_usuario($_POST['data']);
			break;
		}
		case 'obtener_datos_usuario':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->select_info_usuario($_POST['id_usuario']);
			break;
		}
		case 'actualizar_datos_usuario':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->update_info_usuario($_POST['data']);
			break;
		}

		case 'get_pregunta_seguridad':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->obtener_pregunta_seguridad($_POST['correo']);
			break;
		}

		case 'validar_respuesta':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->validar_respuesta($_POST['data']);
			break;
		}

		case 'cambiar_contrasenia':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->cambiar_contrasenia($_POST['data']);
			break;
		}

		case 'obtener_desempenio':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->obtener_desempenio($_POST['id_usuario']);
			break;
		}

		case 'actualizar_sesion':{
			require_once '../controller/usuario_controller.php';
			$controller = new Usuarios();
			$controller->actualizar_sesion($_POST['data']);
			break;
		}
	}
?>