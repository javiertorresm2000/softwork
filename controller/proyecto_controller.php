<?php
    class Proyectos{
        private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
        );
        

        public function __construct(){
			require_once '../model/Proyecto.php';
			$this->model = new Proyecto();
        }

        public function nuevo_proyecto($data){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registrada con Exito";
            $this->response["data"] 		= $this->model->insert_nuevo_proyecto($data);
            
	    	echo json_encode($this->response);
        }

        public function actualizar_proyecto($data){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registrada con Exito";
            $this->response["data"] 		= $this->model->update_proyecto($data);
            
	    	echo json_encode($this->response);
        }

        public function listar_proyectos(){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros";
            $this->response["data"] 		= $this->model->select_proyectos();
            
	    	echo json_encode($this->response);
        }

        public function listar_mis_proyectos($id_usuario){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->select_todos_mis_proyectos($id_usuario);
            
	    	echo json_encode($this->response);
        }

        public function listar_mis_proyectos_activos($id_usuario){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->select_mis_proyectos_activos($id_usuario);
            
	    	echo json_encode($this->response);
        }

        public function listar_mis_proyectos_en_proceso($id_usuario){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->select_mis_proyectos_en_proceso($id_usuario);
            
	    	echo json_encode($this->response);
        }

        public function listar_mis_proyectos_inactivos($id_usuario){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->select_mis_proyectos_inactivos($id_usuario);
            
	    	echo json_encode($this->response);
        }

        public function listar_mis_proyectos_finalizados($id_usuario){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->select_mis_proyectos_finalizados($id_usuario);
            
	    	echo json_encode($this->response);
        }

        public function detalle_proyecto($id_proyecto){
            if($_GET['type']=="explore"){
                $this->response["status"] 		= "ok";
                $this->response["body"] 		= "Registros encontrados";
                $this->response["data"] 		= $this->model->select_proyecto_especifico_explore($id_proyecto);
            }else{
                $this->response["status"] 		= "ok";
                $this->response["body"] 		= "Registros encontrados";
                $this->response["data"] 		= $this->model->select_proyecto_especifico_profile($id_proyecto);
            }
            
            
	    	echo json_encode($this->response);
        }

        public function habilitar_proyecto($id_proyecto){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros Actualizados";
            $this->response["data"] 		= $this->model->update_habilitar_proyecto($id_proyecto);
            
	    	echo json_encode($this->response);
        }

        public function deshabilitar_proyecto($id_proyecto){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros Actualizados";
            $this->response["data"] 		= $this->model->update_deshabilitar_proyecto($id_proyecto);
            
	    	echo json_encode($this->response);
        }

        public function finalizar_proyecto($id_proyecto){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros Actualizados";
            $this->response["data"] 		= $this->model->update_finalizar_proyecto($id_proyecto);
            
	    	echo json_encode($this->response);
        }

        public function eliminar_proyecto($id_proyecto){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros Eliminados";
            $this->response["data"] 		= $this->model->select_proyecto_especifico($id_proyecto);
            
	    	echo json_encode($this->response);
        }

        
    }


?>