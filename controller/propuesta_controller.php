<?php
    class Propuestas{
        private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
        );
        

        public function __construct(){
			require_once '../model/Propuesta.php';
			$this->model = new Propuesta();
        }
        
        public function nueva_propuesta($data){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registrada con Exito";
            $this->response["data"] 		= $this->model->insert_nueva_propuesta($data);
            
	    	echo json_encode($this->response);
        }

        public function listar_propuestas($id_proyecto){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->select_propuestas_proyecto($id_proyecto);
            
	    	echo json_encode($this->response);
        }

        public function listar_mis_propuestas($id_usuario){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->select_mis_propuestas($id_usuario);
            
	    	echo json_encode($this->response);
        }

        public function detalle_propuesta($id_propuesta){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->select_propuesta_especifica($id_propuesta);
            
	    	echo json_encode($this->response);
        }

        public function propuesta_revisada($id_propuesta){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->update_propuesta_revisada($id_propuesta);
            
	    	echo json_encode($this->response);
        }

        public function aceptar_propuesta($id_propuesta){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->update_aceptar_propuesta($id_propuesta);
            
	    	echo json_encode($this->response);
        }

        public function rechazar_propuesta($id_propuesta){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
            $this->response["data"] 		= $this->model->update_rechazar_propuesta($id_propuesta);
            
	    	echo json_encode($this->response);
        }

        public function habilitar_propuesta_rechazada($id_propuesta){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros actualizados";
            $this->response["data"] 		= $this->model->update_habilitar_propuesta_rechazada($id_propuesta);
            
	    	echo json_encode($this->response);
        }


    }
?>