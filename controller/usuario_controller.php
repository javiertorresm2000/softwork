<?php
    class Usuarios{
        private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
        );

        private $response2 = array(
        );


        public function __construct(){
			require_once '../model/Usuario.php';
			$this->model = new Usuario();
        }

        public function nuevo_usuario($data){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registrada con Exito";
            $this->response["data"] 		= $this->model->insert_nuevo_usuario($data);
            
	    	echo json_encode($this->response);
        }

        public function select_info_usuario($id_usuario){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Consulta Exitosa";
            $this->response["data"] 		= $this->model->select_usuario($id_usuario);
            
	    	echo json_encode($this->response);
        }

        public function update_info_usuario($data){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Actualizado con Exito";
            $this->response["data"] 		= $this->model->update_usuario($data);
            
	    	echo json_encode($this->response);
        }

        public function validar_correo($data){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Validado con exito!";
            $this->response["data"] 		= $this->model->validar_correo_no_registrado($data);
            
	    	echo json_encode($this->response);
        }

        public function login($data){
            /*$this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Loggeado!";
            $this->response["data"] 		= $this->model->login_usuario($data);*/

            $this->response2 		= $this->model->login_usuario($data);
            
	    	echo json_encode($this->response2);
        }

        public function cambiar_contrasenia($data){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Datos Actualizados!";
            $this->response["data"] 		= $this->model->update_contrasenia($data);
            
	    	echo json_encode($this->response);
        }

        public function obtener_pregunta_seguridad($correo){
            $this->response["status"] 		= "ok";
            $this->response["body"] 		= "Encontrado con Exito!";
            $this->response["data"] 		= $this->model->select_pregunta_seguridad($correo);
            
            echo json_encode($this->response);
        }

        public function validar_respuesta($data){
            $this->response["status"] 		= "ok";
            $this->response["body"] 		= "Validado con Exito!";
            $this->response["data"] 		= $this->model->validar_respuesta_seguridad($data);
            
            echo json_encode($this->response);
        }

        public function obtener_desempenio($id_usuario){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros Eliminados";
            $this->response["data"] 		= $this->model->select_desempenio_usuario($id_usuario);
            
	    	echo json_encode($this->response);
        }

        public function actualizar_sesion($data){
            $this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Sesion Actualizada";
            $this->response["data"] 		= $this->model->actualizar_tiempo_sesion($data);
            echo json_encode($this->response);
        }
    }

?>