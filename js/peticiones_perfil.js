var urlActual = "http://localhost/softwork/";

//var urlActual = "http://localhost/softwork/";

var status_proyecto = "";
var id_proyecto_detalle = "";


////////// PETICIONES PARA ACCIONES CON MIS PROYECTOS Y PROYECTOS ESPECIFICOS \\\\\\\\\\
function guardar_nuevo_proyecto(data){
    $.ajax({
        url: urlActual + "routes/?route=proyecto_route&controller=guardar_nuevo_proyecto",
        type: "POST",
        dataType: "JSON",
        data:{
            data : data
        },
        beforeSend: function(){
            
        },
        success: function (response){
            if(response.data != "" || response.data != null){
                Swal.fire({
                    icon: 'success',
                    title: 'Yeei!...',
                    text: 'Proyecto publicado!',
                    showConfirmButton: false,
                });
                listar_mis_proyectos_activos(id_usuario);
                $("#panel_informacion").hide(200);
                $("#panel_proyectos").show(200);
                $("#panel_crear_proyecto").hide(200);
                $("#panel_propuestas").hide(200);
                $("#panel_detalle_proyecto").hide(200);
            }
            else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Ocurrio un error!'
                });
            }
            
        }
    });
}

function listar_mis_proyectos_activos(id_usuario){
    $.ajax({
        url: urlActual + "routes/?route=proyecto_route&controller=listar_mis_proyectos_activos",
        type: "POST",
        dataType: "JSON",
        data:{
            id_usuario : id_usuario
        },
        beforeSend: function(){
            $("#proyectos_container_activos").html("");
            $("#proyectos_container_proceso").html("");
            $("#proyectos_container_finalizados").html("");
            $("#proyectos_container_inactivos").html("");
        },
        success: function (response){
            if(response.data){
                for(var i=0; i<response.data.length; i++){
                    var row = response.data[i];

                    switch (row.status){
                        case "1":{
                            $("#proyectos_container_activos").append(
                                '<div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto" style="margin-bottom:25px;">'+
                                    '<div class="panel_container" style="background-color:#00de8c !important">'+
                                        '<div style="width: 100%; text-align:left; height:75%; padding: 10px 20px;">'+
                                            '<h3 style="color: #222 !important;"><b>PROYECTO</b></h3>'+
                                            '<h5 style="color: #555 !important;">'+row.titulo+'</h5>'+
                                        '</div>'+
                                        '<div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">'+
                                        '<button type="button" class="button ver_mas" onclick="detalles_proyecto('+row.id_proyecto+')">Ver Detalles</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            break;
                        }
                        case "3":{
                            $("#proyectos_container_proceso").append(
                                '<div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto" style="margin-bottom:25px;">'+
                                    '<div class="panel_container">'+
                                        '<div style="width: 100%; text-align:left; height:75% !important; padding: 10px 20px;">'+
                                            '<h3 style="color: #222 !important;"><b>PROYECTO</b></h3>'+
                                            '<h5 style="color: #555 !important;">'+row.titulo+'</h5>'+
                                        '</div>'+
                                        '<div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">'+
                                        '<button type="button" class="button ver_mas" onclick="detalles_proyecto('+row.id_proyecto+')">Ver Detalles</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            break;
                        }
                        case "4":{
                            $("#proyectos_container_finalizados").append(
                                '<div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto" style="margin-bottom:25px;">'+
                                    '<div class="panel_container" style="background-color:#75bff3 !important">'+
                                        '<div style="width: 100%; text-align:left; height:75% !important; padding: 10px 20px;">'+
                                            '<h3 style="color: #222 !important;"><b>PROYECTO</b></h3>'+
                                            '<h5 style="color: #555 !important;">'+row.titulo+'</h5>'+
                                        '</div>'+
                                        '<div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">'+
                                        '<button type="button" class="button ver_mas" onclick="detalles_proyecto('+row.id_proyecto+')">Ver Detalles</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            break;
                        }
                        case "2":{
                            $("#proyectos_container_inactivos").append(
                                '<div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto" style="margin-bottom:25px;">'+
                                    '<div class="panel_container" style="background-color:#ff8560 !important">'+
                                        '<div style="width: 100%; text-align:left; height:75% !important; padding: 10px 20px;">'+
                                            '<h3 style="color: #222 !important;"><b>PROYECTO</b></h3>'+
                                            '<h5 style="color: #555 !important;">'+row.titulo+'</h5>'+
                                        '</div>'+
                                        '<div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">'+
                                        '<button type="button" class="button ver_mas" onclick="detalles_proyecto('+row.id_proyecto+')">Ver Detalles</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            break;
                        }
                        default:break;
                    }
                        
                }
            }
            
        }
    });
}

function obtener_proyecto_especifico(id){
    $.ajax({
        url: urlActual + "routes/?route=proyecto_route&controller=detalle_proyecto&type=explore",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
            limpiar_vista_detalle();
            $("#btn_accion_proyecto").html("");
        },
        success: function (response){
            if(response.data){
                data = response.data[0];
                status_proyecto = data.status;
                id_proyecto_detalle = id;
                
                switch (status_proyecto){
                    
                    case "1":{
                        
                        $("#btn_accion_proyecto").append(
                            '<div class="col-md-12 col-sm-12" style="text-align: center;">'+
                                '<button type="button" class="button" id="inhabilitar_proyecto" onclick="deshabilitar_proyecto('+id_proyecto_detalle+')">DESHABILITAR PROYECTO</button>'+
                            '</div>'
                        );
                        break;
                    }
                    case "2":{
                        $("#btn_accion_proyecto").append(
                            '<div class="col-md-12 col-sm-12" style="text-align: center;">'+
                                '<button type="button" class="button" id="inhabilitar_proyecto" onclick="habilitar_proyecto('+id_proyecto_detalle+')">HABILITAR PROYECTO</button>'+
                            '</div>'
                        );
                        break;
                    }
                    case "3":{
                        $("#btn_accion_proyecto").append(
                            '<div class="col-md-12 col-sm-12" style="text-align: center;">'+
                                '<button type="button" class="button" id="inhabilitar_proyecto" onclick="finalizar_proyecto('+id_proyecto_detalle+')">FINALIZAR PROYECTO</button>'+
                            '</div>'
                        );
                        break;
                    }
                    default:break;
                }

                $("#titulo_proyecto_detalle").append(data.titulo);

                $("#descripcion_proyecto_detalle").html(data.descripcion_general);
                $("#observaciones_proyecto_detalle").html(data.observaciones_tecnicas);
                $("#restricciones_proyecto_detalle").html(data.restricciones);

                $("#presupuesto_proyecto_detalle").append(
                    '<p style="color: #444;">Minimo: $ '+data.presupuesto_minimo+'</p>'+
                    '<p style="color: #444;">Maximo: $ '+data.presupuesto_maximo+'</p>'
                );

                $("#fecha_entrega_proyecto_detalle").html(data.fecha_entrega_tentativa);
                
            }
            obtener_propuestas_mi_proyecto(id);
        }
    });
}

function obtener_propuestas_mi_proyecto(id){
    $.ajax({
        url: urlActual + "routes/?route=propuesta_route&controller=listar_propuestas",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
            $("#id_propuesta_aceptada").val("");
            $("#body_table_propuestas").html("");
            $("#panel_propuesta_aceptada").html("");
        },
        success: function (response){

            if(response.data.propuesta != ""){
                var data = response.data.propuesta;
                $("#id_propuesta_aceptada").val(data.id_propuesta);
                $("#panel_propuesta_aceptada").append(
                    '<div class="row panel_propuesta_aceptada" style="width:100% !important; min-height: 50px; color:#444;">'+
                        '<div class="col-md-2 col-sm-4" style="text-align: center;">'+
                            '<img style="height: 100px; width: auto;" class="imagen_feed" src="../images/avatar-01.jpg" id="avatar_usuario">'+
                        '</div>'+
                        '<div class="col-md-6 col-sm-8" style="padding-top: 20px;">'+
                            '<h5 id="nombre_usuario_propuesta_aceptada" style="font-weight: 600; color: #444;">'+data.nombre_propietario+'</h4>'+
                                '<h5 id="carrera_usuario_propuesta_aceptada" style="color: #888;">'+data.especialidad+'</h5>'+
                        '</div>'+
                        '<div class="col-md-4 col-sm-12" style="padding-left: 50px; border-left: 1px solid #cdcdcd">'+
                            '<p class="titulos_proyecto">FECHA DE ENTREGA</p>'+
                            '<div id="fecha_entrega_propuesta_aceptada" style="width:100% !important; min-height: 20px; color:#444;">'+data.fecha_entrega+'</div>'+
                            '<p class="titulos_proyecto">COSTO FINAL</p>'+
                            '<div id="presupuesto_propuesta_aceptada" style="width:100% !important; min-height: 20px; color:#444;">'+data.cotizacion+'</div>'+
                        '</div>'+
                        '<div class="col-md-7 col-sm-12">'+
                            '<hr style="margin-bottom: 25px;">'+
                        '</div>'+
                        '<div class="col-md-5">'+
                        '</div>'+
                        '<div class="col-md-12 col-sm-12">'+
                            '<p class="titulos_proyecto">DESCRIPCION DE LA PROPUESTA</p>'+
                            '<div id="descripcion_propuesta_aceptada" style="width:100% !important; min-height: 70px; color:#444;">'+data.descripcion_general+'</div>'+
                        '</div>'+
                        '<div class="col-md-12 col-sm-12">'+
                            '<p class="titulos_proyecto">ESPECIFICACIONES TECNICAS</p>'+
                            '<div id="especificaciones_propuesta_aceptada" style="width:100% !important; min-height: 50px; color:#444;">'+data.especificacion_tecnica+'</div>'+
                        '</div>'+
                    '</div>'
                );
            }
            else{
                console.log(status_proyecto);
                if(status_proyecto == "2"){
                    $("#id_propuesta_aceptada").val("");
                    $("#panel_propuesta_aceptada").append(
                        '<div class="row" style="width:100%;">'+
                            '<div class="col-md-12 col-sm-2" style="text-align:center;"><h3 style="font-weight: 700px; color:rgba(18, 77, 107, 0.959) !important;">NECESITAS HABILITAR EL PROYECTO PARA PODER ESCOGER ALGUNA PROPUESTA</h3></div>'+
                        '</div>'
                    );
                }else{
                    $("#id_propuesta_aceptada").val("");
                    $("#panel_propuesta_aceptada").append(
                        '<div class="row" style="width:100%;">'+
                            '<div class="col-md-12 col-sm-2" style="text-align:center;"><h3 style="font-weight: 700px; color:rgba(18, 77, 107, 0.959) !important;">NO SE HA ACEPTADO NINGUN PROPUESTA</h3></div>'+
                        '</div>'
                    );
                }
            }

            for(var i = 0; i<response.data.propuestas.length; i++){
                row = response.data.propuestas[i];
                if(row.descripcion_general.length > 150){
                    desc = row.descripcion_general.substr(0,150);
                }
                else{
                    desc =row.descripcion_general;
                }
                switch(row.status){
                    case "1":{
                        row.status = "NEW";
                        break;
                    }
                    case "2":{
                        row.status = "CHECKED";
                        break;
                    }
                    case "3":{
                        row.status = "REJECTED";
                        break;
                    }
                    case "4":{
                        row.status = "ACCEPTED";
                        break;
                    }
                }
                $("#body_table_propuestas").append(
                    '<tr onclick="detalle_propuesta_mi_proyecto('+row.id_propuesta+')">'+
                        '<td class="col-md-2 col-xs-2">'+row.nombre_propietario+'</td>'+
                        '<td class="col-md-2 col-xs-2">'+row.fecha_creacion+'</td>'+
                        '<td class="col-md-4 col-xs-4">'+desc+'...</td>'+
                        '<td class="col-md-2 col-xs-2">'+row.cotizacion+'</td>'+
                        '<td class="col-md-2 col-xs-2">'+row.status+'</td>'+
                    '</tr>'	
                );
            }
        }
    });
}

function deshabilitar_proyecto(id_proyecto_detalle){
    Swal.fire({
        title: 'Estas seguro?',
        text: "Deshabilitaras este proyecto y no estará disponible para los freelancers!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, Deshabilitar!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: urlActual + "routes/?route=proyecto_route&controller=deshabilitar_proyecto",
                type: "POST",
                dataType: "JSON",
                data: {
                    id : id_proyecto_detalle
                },
                beforeSend: function(){
                    limpiar_vista_detalle();
                    $("#btn_accion_proyecto").html("");
                },
                success: function (response){
                    if(response.data=="Actualizado"){
                        Swal.fire({
                            icon: 'success',
                            title: 'Exito!...',
                            text: 'Proyecto inhabilitado!'
                        });
                        
                        listar_mis_proyectos_activos(id_usuario);
                        obtener_proyecto_especifico(id_proyecto_detalle);
                    }
                    else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Ooops...',
                            text: 'No se ha podido inhabilitar este proyecto!'
                        });
                    }
                }
            });
        }
    });
    

}

function habilitar_proyecto(id_proyecto_detalle){
    Swal.fire({
        title: 'Estas seguro?',
        text: "Volverá a estar disponible este proyecto para todos!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, Habilitar!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: urlActual + "routes/?route=proyecto_route&controller=habilitar_proyecto",
                type: "POST",
                dataType: "JSON",
                data: {
                    id : id_proyecto_detalle
                },
                beforeSend: function(){
                    $("#btn_accion_proyecto").html("");
                },
                success: function (response){
                    if(response.data=="Actualizado"){
                        Swal.fire({
                            icon: 'success',
                            title: 'Exito!...',
                            text: 'Proyecto Habilitado!'
                        });
                        
                        listar_mis_proyectos_activos(id_usuario);
                        obtener_proyecto_especifico(id_proyecto_detalle);
                    }
                    else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Ooops...',
                            text: 'No se ha podido Habilitar este proyecto!'
                        });
                    }
                }
            });
        }
    });
    

}

function finalizar_proyecto(id_proyecto_detalle){
    Swal.fire({
        title: 'Estas seguro?',
        text: "El freelancer debio de ya haberte el proyecto final para marcar este proyecto como finalizado!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, Finalizar!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: urlActual + "routes/?route=proyecto_route&controller=finalizar_proyecto",
                type: "POST",
                dataType: "JSON",
                data: {
                    id : id_proyecto_detalle
                },
                beforeSend: function(){
                    $("#btn_accion_proyecto").html("");
                },
                success: function (response){
                    if(response.data=="Actualizado"){
                        Swal.fire({
                            icon: 'success',
                            title: 'Yeei!...',
                            text: 'Proyecto Finalizado!'
                        });
                        
                        listar_mis_proyectos_activos(id_usuario);
                        obtener_proyecto_especifico(id_proyecto_detalle);
                    }
                    else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Ooops...',
                            text: 'No se ha podido Habilitar este proyecto!'
                        });
                    }
                }
            });
        }
    });
    

}
////////// PETICIONES PARA ACCIONES CON MIS PROYECTOS Y PROYECTOS ESPECIFICOS \\\\\\\\\\



/////////// PETICIONES PARA ACCIONES CON LAS PROPUESTAS DE UNO DE MIS PROYECTOS ESPECIFICOS \\\\\\\\\
function obtener_detalle_propuesta(id){
    $.ajax({
        url: urlActual + "routes/?route=propuesta_route&controller=detalle_propuesta",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
        },
        success: function (response){
            if(response.data != ""){
                var data = response.data;
                
                $("#nombre_usuario_propuesta_detalle").html(data.nombre_propietario);
                $("#carrera_usuario_propuesta_detalle").html(data.especialidad);
                $("#descripcion_propuesta_detalle").html(data.descripcion_general);
                $("#especificaciones_propuesta_detalle").html(data.especificacion_tecnica);
                $("#fecha_entrega_propuesta_detalle").html(data.fecha_entrega);
                $("#presupuesto_propuesta_detalle").html("$ "+data.cotizacion);
                $("#id_propuesta_detalle").val(data.id_propuesta);

                if(status_proyecto==1){
                    if(data.status==1 || data.status==2){
                        $("#footer_modal").html("");
                        $("#footer_modal").append(
                            '<button type="button" id="rechazar_propuesta" onclick="btn_rechazar_propuesta()" class="btn btn-danger swal2-styled" style="display: inline-block;">Rechazar</button>'+
                            '<button type="button" id="aceptar_propuesta" onclick="btn_aceptar_propuesta()" class="btn btn-success swal2-styled" style="display: inline-block;">Confirmar</button>'+
                            '<button type="button" data-dismiss="modal" id="close_modal_detalle_propuesta" class="btn btn-info swal2-styled">Cerrar</button>'
                        );
                        
                        actualizar_status_propuesta_visto(id);
                    }else{
                        if(data.status==3){
                            $("#footer_modal").html("");
                            $("#footer_modal").append(
                                '<button type="button" id="habilitar_propuesta" onclick="btn_habilitar_propuesta()" class="btn btn-danger swal2-styled" style="display: inline-block;">Habilitar</button>'+
                                '<button type="button" data-dismiss="modal" id="close_modal_detalle_propuesta" class="btn btn-info swal2-styled">Cerrar</button>'
                            );
                        }else{
                            $("#footer_modal").html("");
                            $("#footer_modal").append(
                                '<button type="button" data-dismiss="modal" id="close_modal_detalle_propuesta" class="btn btn-info swal2-styled">Cerrar</button>'
                            );
                        }
                    }
                }else if(status_proyecto == 2 || status_proyecto == 3 || status_proyecto == 4){

                    $("#footer_modal").html("");
                    $("#footer_modal").append(
                        '<button type="button" data-dismiss="modal" id="close_modal_detalle_propuesta" class="btn btn-info swal2-styled">Cerrar</button>'
                    );
                    
                }

                
            }
        }
    });
}

function aceptar_propuesta(id){
    $.ajax({
        url: urlActual + "routes/?route=propuesta_route&controller=aceptar_propuesta",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
        },
        success: function (response){
            if(response.data.proyecto != "0"){
                
                $("#modal_detalle_propuesta").modal("hide");
                Swal.fire({
                    icon: 'success',
                    title: 'Yeei...',
                    text: 'Propuesta Aceptada!'
                });
                status_proyecto = 2;
                obtener_propuestas_mi_proyecto(response.data.proyecto);

            }
        }
    });
}

function rechazar_propuesta(id){
    $.ajax({
        url: urlActual + "routes/?route=propuesta_route&controller=rechazar_propuesta",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
        },
        success: function (response){
            if(response.data == "Actualizado"){
                
                $("#modal_detalle_propuesta").modal("hide");
                Swal.fire({
                    icon: 'success',
                    title: 'Correcto...',
                    text: 'Propuesta Rechazada!'
                });
                obtener_propuestas_mi_proyecto(id_proyecto_detalle);
            }
            else{
                Swal.fire({
                    icon: 'error',
                    title: 'Ooops...',
                    text: 'Ocurrio un error al intentar rechazar esta propuesta!'
                });
            }
        }
    });
}

function habilitar_propuesta_rechazada(id){
    $.ajax({
        url: urlActual + "routes/?route=propuesta_route&controller=habilitar_propuesta_rechazada",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
        },
        success: function (response){
            if(response.data == "Actualizado"){
                Swal.fire({
                    icon: 'success',
                    title: 'Yeei...',
                    text: 'Propuesta Habilitada!'
                });
                obtener_detalle_propuesta(id);
                obtener_propuestas_mi_proyecto(id_proyecto_detalle);
            }
            else{
                Swal.fire({
                    icon: 'error',
                    title: 'Ooops...',
                    text: 'Esta propuesta no se puede habilitar de nuevo!'
                });
            }
                
            
        }
    });
}

function actualizar_status_propuesta_visto(id){
    $.ajax({
        url: urlActual + "routes/?route=propuesta_route&controller=propuesta_revisada",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
        },
        success: function (response){
            obtener_propuestas_mi_proyecto(response.data.proyecto);
        }
    });
}

/////////// PETICIONES PARA ACCIONES CON LAS PROPUESTAS DE UNO DE MIS PROYECTOS ESPECIFICOS \\\\\\\\\



////////// PETICIONES PARA ACTUALIZAR Y VER LOS DETALLES DEL PERFIL DEL USUARIO LOGGEADO \\\\\\\\\\\
function detalles_perfil(){
    $.ajax({
        url: urlActual + "routes/?route=usuario_route&controller=obtener_datos_usuario",
        type: "POST",
        dataType: "JSON",
        data:{
            id_usuario : id_usuario
        },
        beforeSend: function(){
            $("#nombre_usuario").html("");
            $("#correo_usuario").html("");
        },
        success: function (response){
            if(response.data){
                var row = response.data;
                $("#nombre_usuario").append(row.nombre+" "+row.apellidos);
                $("#correo_usuario").append(row.correo);

                $("#act_nombre").val(row.nombre);
                $("#act_apellidos").val(row.apellidos);
                $("#act_telefono").val(row.telefono);
                $("#act_fecha_nacimiento").val(row.fecha_nacimiento);
                $("#act_genero").val(row.genero);
                $("#act_direccion").val(row.direccion);
                $("#act_descripcion_personal").val(row.descripcion_personal);
                $("#act_municipio").val(row.municipio);
                $("#act_estado").val(row.estado);

                $("#act_grado_academico").val(row.grado_academico);
                $("#act_carrera").val(row.carrera);
                $("#act_experiencia_profesional").val(row.experiencia_profesional);

                
                $("#act_fecha_nacimiento").css('white-space','inherit');
                
            }
        }
    });
}

function actualizar_perfil(data){
    $.ajax({
        url: urlActual + "routes/?route=usuario_route&controller=actualizar_datos_usuario",
        type: "POST",
        dataType: "JSON",
        data:{
            data : data
        },
        beforeSend: function(){
            
        },
        success: function (response){
            if(response.data == "Exito"){
                Swal.fire({
                    icon: 'success',
                    title: 'Yeei!...',
                    text: 'Datos actualizados con éxito!'
                });
                detalles_perfil();
            }
            else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Ocurrio un error!'
                });
            }
        }
    });
}
////////// PETICIONES PARA ACTUALIZAR Y VER LOS DETALLES DEL PERFIL DEL USUARIO LOGGEADO \\\\\\\\\\\




////////// PETICIONES PARA OBTENER LAS PROPUESTAS QUE HE HECHO A DIFERENTES PROYECTOS \\\\\\\\\\\
function listar_mis_propuesta(id_usuario){
    $.ajax({
        url: urlActual + "routes/?route=propuesta_route&controller=listar_mis_propuestas",
        type: "POST",
        dataType: "JSON",
        data: {
            id_usuario : id_usuario
        },
        beforeSend: function(){
            $("#propuestas_container_activas").html("");
            $("#propuestas_container_aceptadas").html("");
            $("#propuestas_container_rechazadas").html("");
        },
        success: function (response){
            if(response.data){
                for(var i=0; i<response.data.length; i++){
                    var row = response.data[i];

                    switch (row.status){
                        case "1":{
                            $("#propuestas_container_activas").append(
                                '<div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto"  style="margin-bottom:25px;">'+
                                    '<div class="panel_container" style="background-color:#00de8c !important">'+
                                        '<div style="width: 100%; text-align:left; height:75%; padding: 10px 20px;">'+
                                            '<h3 style="color: #222 !important;"><b>PROPUESTA</b></h3>'+
                                            '<h5 style="color: #555 !important;"><b style="color:#555;">PROYECTO: </b>'+row.titulo_proyecto+'</h5>'+
                                        '</div>'+
                                        '<div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">'+
                                        '<button type="button" class="button ver_mas" onclick="detalles_propuesta('+row.id_propuesta+')">Ver Detalles</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            break;
                        }
                        case "2":{
                            $("#propuestas_container_activas").append(
                                '<div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto"  style="margin-bottom:25px;">'+
                                    '<div class="panel_container">'+
                                        '<div style="width: 100%; text-align:left; height:75% !important; padding: 10px 20px;">'+
                                            '<h3 style="color: #222 !important;"><b>PROPUESTA</b></h3>'+
                                            '<h5 style="color: #555 !important;"><b style="color:#555;">PROYECTO: </b>'+row.titulo_proyecto+'</h5>'+
                                        '</div>'+
                                        '<div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">'+
                                        '<button type="button" class="button ver_mas" onclick="detalles_propuesta('+row.id_propuesta+')">Ver Detalles</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            break;
                        }
                        case "4":{
                            $("#propuestas_container_aceptadas").append(
                                '<div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto"  style="margin-bottom:25px;">'+
                                    '<div class="panel_container" style="background-color:#e7aa00 !important">'+
                                        '<div style="width: 100%; text-align:left; height:75% !important; padding: 10px 20px;">'+
                                            '<h3 style="color: #222 !important;"><b>PROPUESTA</b></h3>'+
                                            '<h5 style="color: #555 !important;"><b style="color:#555;">PROYECTO: </b>'+row.titulo_proyecto+'</h5>'+
                                        '</div>'+
                                        '<h3 style="color: #222 !important;"><b style="color:#666 !important;">ACEPTADA</b></h3>'+
                                        '<div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">'+
                                        '<button type="button" class="button ver_mas" onclick="detalles_propuesta('+row.id_propuesta+')">Ver Detalles</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            break;
                        }
                        case "5":{
                            $("#propuestas_container_aceptadas").append(
                                '<div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto"  style="margin-bottom:25px;">'+
                                    '<div class="panel_container" style="background-color:#75bff3 !important">'+
                                        '<div style="width: 100%; text-align:left; height:75% !important; padding: 10px 20px;">'+
                                            '<h3 style="color: #222 !important;"><b>PROPUESTA</b></h3>'+
                                            '<h5 style="color: #555 !important;"><b style="color:#555;">PROYECTO: </b>'+row.titulo_proyecto+'</h5>'+
                                        '</div>'+
                                        '<h3 style="color: #222 !important;"><b style="color:#666 !important;">FINALIZADA</b></h3>'+
                                        '<div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">'+
                                        '<button type="button" class="button ver_mas" onclick="detalles_propuesta('+row.id_propuesta+')">Ver Detalles</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            break;
                        }
                        case "3":{
                            $("#propuestas_container_rechazadas").append(
                                '<div class="col-lg-4 col-md-6 col-sm-6 panel_proyecto"  style="margin-bottom:25px;">'+
                                    '<div class="panel_container" style="background-color:#ff8560 !important">'+
                                        '<div style="width: 100%; text-align:left; height:75% !important; padding: 10px 20px;">'+
                                            '<h3 style="color: #222 !important;"><b>PROPUESTA</b></h3>'+
                                            '<h5 style="color: #555 !important;"><b style="color:#555;">PROYECTO: </b>'+row.titulo_proyecto+'</h5>'+
                                        '</div>'+
                                        '<div style="background-color:#000; width: 100%; height:25%; border-top: 2px solid white;padding: 10px 15px; text-align:right;">'+
                                        '<button type="button" class="button ver_mas" onclick="detalles_propuesta('+row.id_propuesta+')">Ver Detalles</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            );
                            break;
                        }
                        default:break;
                    }
                        
                }
            }
            
        }
    });
}

function obtener_mi_propuesta_especifica(id){
    $.ajax({
        url: urlActual + "routes/?route=propuesta_route&controller=detalle_propuesta",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
            $("#panel_mi_propuesta").html("");
        },
        success: function (response){
            var data = response.data;
            $("#panel_mi_propuesta").append(
                '<div class="row panel_propuesta_aceptada" style="width:100% !important; min-height: 50px; color:#444;">'+
                    '<div class="col-md-2 col-sm-4" style="text-align: center;">'+
                        '<img style="height: 100px; width: auto;" class="imagen_feed" src="../images/avatar-01.jpg" id="avatar_usuario">'+
                    '</div>'+
                    '<div class="col-md-6 col-sm-8" style="padding-top: 20px;">'+
                        '<h5 id="nombre_usuario_propuesta_aceptada" style="font-weight: 600; color: #444;">'+data.nombre_propietario+'</h4>'+
                            '<h5 id="carrera_usuario_propuesta_aceptada" style="color: #888;">'+data.especialidad+'</h5>'+
                    '</div>'+
                    '<div class="col-md-4 col-sm-12" style="padding-left: 50px; border-left: 1px solid #cdcdcd">'+
                        '<p class="titulos_proyecto">FECHA DE CREACION</p>'+
                        '<div id="fecha_entrega_propuesta_aceptada" style="width:100% !important; min-height: 20px; color:#444;">'+data.fecha_creacion+'</div>'+
                        '<p class="titulos_proyecto">FECHA DE ENTREGA</p>'+
                        '<div id="fecha_entrega_propuesta_aceptada" style="width:100% !important; min-height: 20px; color:#444;">'+data.fecha_entrega+'</div>'+
                        '<p class="titulos_proyecto">COSTO FINAL</p>'+
                        '<div id="presupuesto_propuesta_aceptada" style="width:100% !important; min-height: 20px; color:#444;">'+data.cotizacion+'</div>'+
                    '</div>'+
                    '<div class="col-md-7 col-sm-12">'+
                        '<hr style="margin-bottom: 25px;">'+
                    '</div>'+
                    '<div class="col-md-5">'+
                    '</div>'+
                    '<div class="col-md-12 col-sm-12">'+
                        '<p class="titulos_proyecto">DESCRIPCION DE LA PROPUESTA</p>'+
                        '<div id="descripcion_propuesta_aceptada" style="width:100% !important; min-height: 70px; color:#444;">'+data.descripcion_general+'</div>'+
                    '</div>'+
                    '<div class="col-md-12 col-sm-12">'+
                        '<p class="titulos_proyecto">ESPECIFICACIONES TECNICAS</p>'+
                        '<div id="especificaciones_propuesta_aceptada" style="width:100% !important; min-height: 50px; color:#444;">'+data.especificacion_tecnica+'</div>'+
                    '</div>'+
                '</div>'
            );

            obtener_proyecto_propuesta_especifico(data.proyecto);
        }
    });
}

function obtener_proyecto_propuesta_especifico(id){
    $.ajax({
        url: urlActual + "routes/?route=proyecto_route&controller=detalle_proyecto&type=explore",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
            limpiar_vista_detalle_propuesta()
        },
        success: function (response){
            if(response.data){
                data = response.data[0];


                $("#titulo_proyecto_propuesta").append(data.titulo);

                $("#descripcion_proyecto_propuesta").html(data.descripcion_general);
                $("#observaciones_proyecto_propuesta").html(data.observaciones_tecnicas);
                $("#restricciones_proyecto_propuesta").html(data.restricciones);

                $("#presupuesto_proyecto_propuesta").append(
                    '<p style="color: #444;">Minimo: $ '+data.presupuesto_minimo+'</p>'+
                    '<p style="color: #444;">Maximo: $ '+data.presupuesto_maximo+'</p>'
                );

                $("#fecha_entrega_proyecto_propuesta").html(data.fecha_entrega_tentativa);
                
            }
        }
    });
}