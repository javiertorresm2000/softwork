var loc = window.location;
var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/')+1);
//var urlActual = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));

var urlActual = "http://localhost/softwork/";
var correo_valido = "";
// LOGIN
    function validar_correo(data){
        
        $.ajax({
            url: urlActual + "routes/?route=usuario_route&controller=validar_correo",
			type: "POST",
            dataType: "JSON",
            data: {
                data : data
            },
            beforeSend: function(){
                
            },
            success: function (response){
                
                if(response.data=="0"){
                    correo_valido = "0";
                }else{
                    correo_valido = "1";
                }
            }
        });
    }


    function registrar_nuevo_usuario(data){
        $.ajax({
            url: urlActual + "routes/?route=usuario_route&controller=registrar_nuevo_usuario",
			type: "POST",
            dataType: "JSON",
            data: {
                data : data
            },
            beforeSend: function(){
                
            },
            success: function (response){
                if(response.data != "0"){
                    Swal.fire({
                        icon: 'success',
                        title: 'Yass...',
                        text: 'Registro Exitoso!',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setTimeout(function(){ 
                        window.location='../index.php';
                    }, 1500);
                }
                else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Ocurrio un error al intentar registrar!'
                        //footer: '<a href>Why do I have this issue?</a>'
                    });
                }
                
            }
        });
    }

