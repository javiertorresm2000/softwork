
$(document).ready( function () {
    detalles_perfil();

    $("#panel_informacion").css("display","none");
    $("#panel_proyectos").css("display","none");
    $("#panel_crear_proyecto").css("display","block");
    $("#panel_propuestas").css("display","none");
    $("#panel_detalle_proyecto").css("display","none");
    $("#panel_detalle_propuesta").css("display","none");

    $("#menu_proyectos").removeClass("menu_perfil");
    $("#menu_proyectos").addClass("menu_selected");

    $("#menu_informacion").removeClass("menu_selected");
    $("#menu_informacion").addClass("menu_perfil");
    
    
    $("#menu_propuestas").addClass("menu_perfil");
    $("#menu_propuestas").removeClass("menu_selected");

    listar_mis_proyectos_activos(id_usuario);
});


// FUNCIONES PARA TOGGLEAR LAS OPCIONES DE LA VISTA DE MI PERFIL
    // CAMBIA VISTA AL MENU DE MIS PROYECTO
    $("#menu_proyectos").click(function(){
        $("#panel_informacion").hide(400);
        $("#panel_proyectos").show(400);
        $("#panel_crear_proyecto").hide(400);
        $("#panel_propuestas").hide(400);
        $("#panel_detalle_proyecto").hide(400);
        $("#panel_detalle_propuesta").hide(400);

        $("#menu_proyectos").removeClass("menu_perfil");
        $("#menu_proyectos").addClass("menu_selected");

        $("#menu_informacion").removeClass("menu_selected");
        $("#menu_informacion").addClass("menu_perfil");
        
        
        $("#menu_propuestas").addClass("menu_perfil");
        $("#menu_propuestas").removeClass("menu_selected");

        listar_mis_proyectos_activos(id_usuario);

    });
    // CAMBIA VISTA AL MENU DE INFORMACION PERSONAL
    $("#menu_informacion").click(function(){
        $("#panel_informacion").show(400);
        $("#panel_proyectos").hide(400);
        $("#panel_crear_proyecto").hide(400);
        $("#panel_propuestas").hide(400);
        $("#panel_detalle_proyecto").hide(400);
        $("#panel_detalle_propuesta").hide(400);

        $("#menu_proyectos").addClass("menu_perfil");
        $("#menu_proyectos").removeClass("menu_selected");

        $("#menu_informacion").addClass("menu_selected");
        $("#menu_informacion").removeClass("menu_perfil");
        
        
        $("#menu_propuestas").addClass("menu_perfil");
        $("#menu_propuestas").removeClass("menu_selected");

    });
    // CAMBIA VISTA AL MENU PROPUESTAS
    $("#menu_propuestas").click(function(){
        $("#panel_informacion").hide(400);
        $("#panel_proyectos").hide(400);
        $("#panel_crear_proyecto").hide(400);
        $("#panel_propuestas").show(400);
        $("#panel_detalle_proyecto").hide(400);
        $("#panel_detalle_propuesta").hide(400);

        $("#menu_proyectos").addClass("menu_perfil");
        $("#menu_proyectos").removeClass("menu_selected");

        $("#menu_informacion").removeClass("menu_selected");
        $("#menu_informacion").addClass("menu_perfil");
        
        
        $("#menu_propuestas").removeClass("menu_perfil");
        $("#menu_propuestas").addClass("menu_selected");

        listar_mis_propuesta(id_usuario);

    });
    // CAMBIA VISTA PANEL NUEVO PROYECTO
    $("#crear_nuevo_proyecto").click(function(){
        $("#panel_informacion").hide(400);
        $("#panel_proyectos").hide(400);
        $("#panel_crear_proyecto").show(400);
        $("#panel_propuestas").hide(400);
        $("#panel_detalle_proyecto").hide(400);
        $("#panel_detalle_propuesta").hide(400);
    });
// FUNCIONES PARA TOGGLEAR LAS OPCIONES DE LA VISTA DE MI PERFIL



// CAMBIA VISTA A DETALLE ESPECIFICO PROYECTO
function detalles_proyecto(id){
    $("#panel_informacion").hide(200);
    $("#panel_proyectos").hide(200);
    $("#panel_crear_proyecto").hide(200);
    $("#panel_propuestas").hide(200);
    $("#panel_detalle_proyecto").show(200);
    $("#panel_detalle_propuesta").hide(200);

    obtener_proyecto_especifico(id);
}
function limpiar_vista_detalle(){
    $("#titulo_proyecto_detalle").html("");
    $("#descripcion_proyecto_detalle").html("");
    $("#observaciones_proyecto_detalle").html("");
    $("#restricciones_proyecto_detalle").html("");
    $("#presupuesto_proyecto_detalle").html("");
    $("#fecha_entrega_proyecto_detalle").html("");
}
// CAMBIA VISTA A DETALLE ESPECIFICO PROYECTO

// CAMBIA VISTA A DETALLE ESPECIFICO PROPUESTA
function detalles_propuesta(id){
    $("#panel_informacion").hide(200);
    $("#panel_proyectos").hide(200);
    $("#panel_crear_proyecto").hide(200);
    $("#panel_propuestas").hide(200);
    $("#panel_detalle_proyecto").hide(200);
    $("#panel_detalle_propuesta").show(200);

    obtener_mi_propuesta_especifica(id);
}
function limpiar_vista_detalle_propuesta(){
    $("#titulo_proyecto_propuesta").html("");
    $("#descripcion_proyecto_propuesta").html("");
    $("#observaciones_proyecto_propuesta").html("");
    $("#restricciones_proyecto_propuesta").html("");
    $("#presupuesto_proyecto_propuesta").html("");
    $("#fecha_entrega_proyecto_propuesta").html("");
}
// CAMBIA VISTA A DETALLE ESPECIFICO PROPUESTA

// FUNCIONES PARA REGRESAR ENTRE VISTAS DENTRO DEL PANEL DE MIS PROYECTOS
$("#regresar_panel_mis_proyectos").click(function(){
    $("#panel_informacion").hide(200);
    $("#panel_proyectos").show(200);
    $("#panel_crear_proyecto").hide(200);
    $("#panel_propuestas").hide(200);
    $("#panel_detalle_proyecto").hide(200);
});

$("#regresar_panel_mis_proyectos2").click(function(){
    $("#panel_informacion").hide(200);
    $("#panel_proyectos").show(200);
    $("#panel_crear_proyecto").hide(200);
    $("#panel_propuestas").hide(200);
    $("#panel_detalle_proyecto").hide(200);
});

// FUNCION PARA REGRESAR ENTRE VISTAS DENTRO DEL PANEL DE MIS PROPUESTAS
$("#regresar_panel_mis_propuestas").click(function(){
    $("#panel_informacion").hide(200);
    $("#panel_proyectos").hide(200);
    $("#panel_crear_proyecto").hide(200);
    $("#panel_propuestas").show(200);
    $("#panel_detalle_proyecto").hide(200);
    $("#panel_detalle_propuesta").hide(200);
});


// FUNCIONES PARA TOGGLEAR EL HEADER DE MIS PROYETCOS
function show_proyectos_activos(){
    $("#header_proy_activos").addClass("header_selected");
    $("#header_proy_proceso").removeClass("header_selected");
    $("#header_proy_finalizados").removeClass("header_selected");
    $("#header_proy_inactivos").removeClass("header_selected");
    /*
    $("#proyectos_container_activos").css("display","flex");
    $("#proyectos_container_proceso").css("display","none");
    $("#proyectos_container_finalizados").css("display","none");
    $("#proyectos_container_inactivos").css("display","none");
    */

    $("#proyectos_container_activos").show(300);
    $("#proyectos_container_activos").css("display","flex");
    $("#proyectos_container_proceso").hide(300);
    $("#proyectos_container_finalizados").hide(300);
    $("#proyectos_container_inactivos").hide(300);
}
function show_proyectos_proceso(){
    $("#header_proy_activos").removeClass("header_selected");
    $("#header_proy_proceso").addClass("header_selected");
    $("#header_proy_finalizados").removeClass("header_selected");
    $("#header_proy_inactivos").removeClass("header_selected");

    $("#proyectos_container_activos").hide(300);
    $("#proyectos_container_proceso").show(300);
    $("#proyectos_container_proceso").css("display","flex");
    $("#proyectos_container_finalizados").hide(300);
    $("#proyectos_container_inactivos").hide(300);
}
function show_proyectos_finalizados(){
    $("#header_proy_activos").removeClass("header_selected");
    $("#header_proy_proceso").removeClass("header_selected");
    $("#header_proy_finalizados").addClass("header_selected");
    $("#header_proy_inactivos").removeClass("header_selected");

    $("#proyectos_container_activos").hide(300);
    $("#proyectos_container_proceso").hide(300);
    $("#proyectos_container_finalizados").show(300);
    $("#proyectos_container_finalizados").css("display","flex");
    $("#proyectos_container_inactivos").hide(300);
}
function show_proyectos_inactivos(){
    $("#header_proy_activos").removeClass("header_selected");
    $("#header_proy_proceso").removeClass("header_selected");
    $("#header_proy_finalizados").removeClass("header_selected");
    $("#header_proy_inactivos").addClass("header_selected");

    $("#proyectos_container_activos").hide(300);
    $("#proyectos_container_proceso").hide(300);
    $("#proyectos_container_finalizados").hide(300);
    $("#proyectos_container_inactivos").show(300);
    $("#proyectos_container_inactivos").css("display","flex");
    
}
// FUNCIONES PARA TOGGLEAR EL HEADER DE MIS PROYETCOS



// FUNCIONES PARA TOGGLEAR EL HEADER DE MIS PROPUESTAS
function show_propuestas_activas(){
    $("#header_prop_activas").addClass("header_selected");
    $("#header_prop_aceptadas").removeClass("header_selected");
    $("#header_prop_rechazadas").removeClass("header_selected");

    $("#propuestas_container_activas").show(300);
    $("#propuestas_container_activas").css("display","flex");
    $("#propuestas_container_aceptadas").hide(300);
    $("#propuestas_container_rechazadas").hide(300);
}
function show_propuestas_aceptadas(){
    $("#header_prop_activas").removeClass("header_selected");
    $("#header_prop_aceptadas").addClass("header_selected");
    $("#header_prop_rechazadas").removeClass("header_selected");

    $("#propuestas_container_activas").hide(300);
    $("#propuestas_container_aceptadas").show(300);
    $("#propuestas_container_aceptadas").css("display","flex");
    $("#propuestas_container_rechazadas").hide(300);

}
function show_propuestas_rechazadas(){
    $("#header_prop_activas").removeClass("header_selected");
    $("#header_prop_aceptadas").removeClass("header_selected");
    $("#header_prop_rechazadas").addClass("header_selected");

    $("#propuestas_container_activas").hide(300);
    $("#propuestas_container_aceptadas").hide(300);
    $("#propuestas_container_rechazadas").show(300);
    $("#propuestas_container_rechazadas").css("display","flex");
    
    
}
// FUNCIONES PARA TOGGLEAR EL HEADER DE MIS PROPUESTAS




// FUNCIONES PARA TOGGLEAR LAS VISTAS DE DETALLE PROYECTO Y PROPUESTAS PROYECTO
function show_info_proyecto(){
    $("#header_detalle_proyecto").addClass("header_selected");
    $("#header_propuestas_proyecto").removeClass("header_selected");

    $("#view_info_proyecto").show(200);
    $("#view_propuestas_proyecto").hide(200);
}
function show_propuestas_proyecto(){
    $("#header_detalle_proyecto").removeClass("header_selected");
    $("#header_propuestas_proyecto").addClass("header_selected");

    $("#view_info_proyecto").hide(200);
    $("#view_propuestas_proyecto").show(200);
}
// FUNCIONES PARA TOGGLEAR LAS VISTAS DE DETALLE PROYECTO Y PROPUESTAS PROYECTO





// FUNCION PARA OBTENER ES DETALLE DE UNA PROPUESTA
function detalle_propuesta_mi_proyecto(id){
    $("#modal_detalle_propuesta").modal("show");
    $("#nombre_usuario_propuesta_detalle").html("");
    $("#carrera_usuario_propuesta_detalle").html("");
    $("#descripcion_propuesta_detalle").html("");
    $("#especificaciones_propuesta_detalle").html("");
    $("#fecha_entrega_propuesta_detalle").html("");
    $("#presupuesto_propuesta_detalle").html("");
    $("#id_propuesta_detalle").val("");
    $("#modal_detalle_propuesta").modal("show");

    obtener_detalle_propuesta(id);
}

// FUNCIONES PARA UNA PROPUESTA
function btn_aceptar_propuesta(){
    Swal.fire({
        title: 'Estas seguro?',
        text: "No podrás hacer cambios después!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, Confirmar!'
    }).then((result) => {
        if (result.value) {
            id = $("#id_propuesta_detalle").val();
            aceptar_propuesta(id);
        }
    });
}

function btn_rechazar_propuesta(){
    Swal.fire({
        title: 'Estas seguro de rechazar esta propuesta?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, Rechazar!'
    }).then((result) => {
        if (result.value) {
            id = $("#id_propuesta_detalle").val();
            rechazar_propuesta(id);
        }
    });
}

function btn_habilitar_propuesta(){
    console.log("Llega");
    Swal.fire({
        title: 'Estas seguro de habilitar esta propuesta?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, Confirmar!'
    }).then((result) => {
        if (result.value) {
            id = $("#id_propuesta_detalle").val();
            habilitar_propuesta_rechazada(id);
        }
    });
}
//

// FUNCIONES PARA EL CONTADOR DE CARACTERES DE LOS TEXTAREAS
function countChars(obj){
    document.getElementById("contador_descripcion").innerHTML = obj.value.length+' /5000';
}
function countChars2(obj){
    document.getElementById("contador_restricciones").innerHTML = obj.value.length+' /5000';
}
function countChars3(obj){
    document.getElementById("contador_observaciones").innerHTML = obj.value.length+' /5000';
}
// FUNCIONES PARA EL CONTADOR DE CARACTERES DE LOS TEXTAREAS


// FUNCION PARA EL SELECT DE TIPO FECHA 
$('[type=date]').change( function() {
    $(this).css('white-space','inherit');
});


// FUNCION PARA OBTENER DATOS DEL USUARIO Y ACTUALIZAR EN CASO DE QUE HAYA CAMBIOS
$("#actualizar_datos").click(function(){
    nombre = $("#act_nombre").val();
    apellidos = $("#act_apellidos").val();
    telefono = $("#act_telefono").val();
    fecha_nacimiento = $("#act_fecha_nacimiento").val();
    genero = $("#act_genero").val();
    direccion = $("#act_direccion").val();
    descripcion_personal = $("#act_descripcion_personal").val();
    municipio = $("#act_municipio").val();
    estado = $("#act_estado").val();

    grado_academico = $("#act_grado_academico").val();
    carrera = $("#act_carrera").val();
    experiencia_profesional = $("#act_experiencia_profesional").val();

    if(nombre == "" || apellidos == "" || telefono == "" || fecha_nacimiento == "" || genero == "" || direccion == "" || descripcion_personal == "" || municipio == "" || estado == ""){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Notamos que dejaste algun campo vacio!'
        });
    }else{

        Swal.fire({
            title: 'Estas seguro de actualizar tus datos?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Actualizar'
        }).then((result) => {
            if (result.value) {
                data = {
                    id_usuario : id_usuario,
                    nombre : nombre,
                    apellidos : apellidos,
                    telefono : telefono,
                    fecha_nacimiento : fecha_nacimiento,
                    genero : genero,
                    direccion : direccion,
                    descripcion_personal : descripcion_personal,
                    municipio : municipio,
                    estado : estado,
                    grado_academico : grado_academico,
                    carrera : carrera,
                    experiencia_profesional : experiencia_profesional
                };
        
                actualizar_perfil(data);
            }
        });
    }
});

// FUNCION PARA OBTENER LOS DATOS DE UN PROYECTO NUEVO
$("#publicar_proyecto").click(function(){
    titulo = $("#titulo_nuevo_proyecto").val();
    descripcion_general = $("#descripcion_nuevo_proyecto").val();
    restricciones = $("#restricciones_nuevo_proyecto").val();
    observaciones = $("#observaciones_nuevo_proyecto").val();
    fecha_entrega = $("#fecha_entrega_nuevo_proyecto").val();
    presupuesto_min = $("#presupuesto_min_nuevo_proyecto").val();
    presupuesto_max = $("#presupuesto_max_nuevo_proyecto").val();

    if(titulo == "" || descripcion_general == "" || restricciones == "" || observaciones == "" || fecha_entrega == ""){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Notamos que dejaste algun campo vacio!'
        });
    }else{
        Swal.fire({
            title: '¿Estas Seguro?',
            text: "Tu proyecto será publicado y no podrás hacer cambios en el",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, publicar!'
        }).then((result) => {
            if (result.value) {
                data = {
                    id_usuario : id_usuario,
                    titulo : titulo,
                    descripcion_general : descripcion_general,
                    restricciones : restricciones,
                    observaciones : observaciones,
                    fecha_entrega : fecha_entrega,
                    presupuesto_min : presupuesto_min,
                    presupuesto_max : presupuesto_max
                };
        
                guardar_nuevo_proyecto(data);
            }
        });
    }
});


