var urlActual = "http://localhost/softwork/";

//var urlActual = "http://localhost/softwork/";

$("#cerrar_sesion").click(function(){
    var fecha_cierre_sesion = new Date();
    minutos = parseInt(fecha_cierre_sesion.getTime() - date_session.getTime()) / 60000;
    data={
        sesion :sesion,
        minutos : minutos
    }
    console.log(minutos);
    $.ajax({
        url: urlActual + "routes/?route=usuario_route&controller=actualizar_sesion",
        type: "POST",
        dataType: "JSON",
        data: {
            data : data
        },
        beforeSend: function(){
        },
        success: function (response){
                window.location = "../index.php";
            
            
        }
    });
});


function listar_proyectos(){
    $.ajax({
        url: urlActual + "routes/?route=proyecto_route&controller=listar_proyectos",
        type: "POST",
        dataType: "JSON",
        beforeSend: function(){
            
        },
        success: function (response){
            if(response.data != ""){
                for(var i=0; i<response.data.length; i++){
                    var row = response.data[i];
                    

                    $("#proyectos-container").append(
                            '<div class="row proyect-container" style=" background-color: #efefef00!important;">'+
                                '<div class="col-sm-12 d-none d-md-block col-md-2">'+
                                    '<img class="imagen_feed" src="../images/avatar-01.jpg" alt="">'+
                                '</div>'+
                                '<div class="col-sm-12 col-md-8">'+
                                    '<h2 style="color: #444; margin: 0px !important; text-align:left; width: auto !important;">'+row.titulo+'</h2>'+
                                    '<hr class="hr_small_header">'+
                                    '<div class="row" style="color: #666 !important; font-weight:normal !important;">'+
                                        '<div class="col-12 pt-3 titulos_proyecto">DESCRIPCION DEL PROYECTO</div>'+
                                        '<div class="col-12">'+row.descripcion_general+'</div>'+
                                        '<div class="col-12 pt-3 titulos_proyecto">FECHA DE ENTREGA (ESTIMADA)</div>'+
                                        '<div class="col-12">'+row.fecha_entrega_tentativa+'</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-sm-12 col-md-2" style="display:flex;">'+
                                    '<div class="row" style="align-items: flex-end;">'+
                                        '<div style="height: 50% !important; vertical-align:top; color:#444;" class="col-md-12 col-sm-12">'+
                                            '<h5 class="titulos_proyecto">PRESUPUESTO</h5> $ '+row.presupuesto_maximo+
                                        '</div>'+
                                        '<div style="height: 50% !important; bottom:0px; padding-bottom:0px !important;" class="col-md-12 col-sm-12 py-2">'+
                                            '<div class="row" style="align-items: flex-end; height: 100% !important; padding: 0px 15px;">'+
                                                '<button type="button" class="button" style="width:100%;" id="abrir_detalle_proyecto" onclick="detalle_proyecto('+row.id_proyecto+')">VER MAS</button>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div style="width: 100%; style="text-align:center !imprtant;">'+
                                '<hr style="width:90%; border:1px solid #777; height: 2px; background-color: #777; margin-left: 5%">'+
                            '</div>'
                    );
                }
            }
            else{
                $("#proyectos_disponibles").html("");
                $("#proyectos_disponibles").append("NO HAY PROYECTOS DISPONIBLES");

            }
            
        }
    });
}

function obtener_proyecto_especifico(id){
    $.ajax({
        url: urlActual + "routes/?route=proyecto_route&controller=detalle_proyecto&type=explore",
        type: "POST",
        dataType: "JSON",
        data: {
            id : id
        },
        beforeSend: function(){
            limpiar_vista_detalle();
        },
        success: function (response){
            if(response.data){
                data = response.data[0];

                $("#titulo_proyecto").append(data.titulo);
                $("#nombre_usuario_proyecto").html(data.nombre_usuario);
                $("#especialidad_usuario").html(data.descripcion_personal);
                $("#descripcion_proyecto").html(data.descripcion_general);
                $("#observaciones_proyecto").html(data.observaciones_tecnicas);
                $("#restricciones_proyecto").html(data.restricciones);
                $("#num_propuestas").html(data.num_propuestas);
                $("#presupuesto_minimo").html(data.presupuesto_minimo);
                $("#presupuesto_maximo").html(data.presupuesto_maximo);
                $("#fecha_tentativa").html(data.fecha_entrega_tentativa);

                $("#id_proyecto").val(id);
                
            }
        }
    });
}

function guardar_propuesta(data){
    var id_proyecto = data.id_proyecto;
    $.ajax({
        url: urlActual + "routes/?route=propuesta_route&controller=guardar_propuesta",
        type: "POST",
        dataType: "JSON",
        data: {
            data:data
        },
        beforeSend: function(){
            
        },
        success: function (response){
            if(response.data != ""){
                Swal.fire({
                    icon: 'success',
                    title: 'Propuesta guardada con éxito!',
                    showConfirmButton: false,
                    timer: 1000
                });
                $("#modal_crear_propuesta").modal("hide");
                obtener_proyecto_especifico(id_proyecto);
            }
            else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Ocurrio un error al procesar!'
                });
            }
        }
    });
}

function obtener_desempeño(id_usuario){
    $.ajax({
        url: urlActual + "routes/?route=usuario_route&controller=obtener_desempenio",
        type: "POST",
        dataType: "JSON",
        data: {
            id_usuario : id_usuario
        },
        beforeSend: function(){
            
        },
        success: function (response){
            if(response.data != ""){
                data = response.data;

                $("#contador_proy_activos").html(data.proy_activos);
                $("#contador_proy_proceso").html(data.proy_proceso);
                $("#contador_proy_finalizados").html(data.proy_finalizados);
                $("#contador_proy_inactivos").html(data.proy_inactivos);

                $("#contador_prop_activas").html(data.prop_activas);
                $("#contador_prop_aceptadas").html(data.prop_aceptadas);
                $("#contador_prop_finalizadas").html(data.prop_finalizadas);
                $("#contador_prop_rechazadas").html(data.prop_rechazadas);
            }
            
        }
    });
}