$(document).ready( function () {
    listar_proyectos();
    obtener_desempeño(id_usuario);
});

function detalle_proyecto(id){
    $("#banner_proyectos").hide(500);
    $("#banner_detalle_proyecto").show(500);
    
    
    $('html, body').animate({scrollTop:0}, 'slow');
    limpiar_vista_detalle();

    obtener_proyecto_especifico(id);

}

function limpiar_vista_detalle(){
    $("#titulo_proyecto").html("");
    $("#nombre_usuario_proyecyo").html("");
    $("#especialidad_usuario").html("");
    $("#descripcion_proyecto").html("");
    $("#observaciones_proyecto").html("");
    $("#restricciones_proyecto").html("");
    $("#num_propuestas").html("");
    $("#presupuesto_minimo").html("");
    $("#presupuesto_maximo").html("");
    $("#fecha_tentativa").html("");

    $("#id_proyecto").val("");
}

function limpiar_modal_propuesta(){
    $("#especificaciones_propuesta").val("");
    $("#descripcion_propuesta").val("");
    $("#fecha_entrega_propuesta").val("");
    $("#presupuesto_propuesta").val("");
}

$("#crear_propuesta").click(function(){
    $("#modal_nueva_propuesta").modal("show");
});

$('[type=date]').change( function() {
    $(this).css('white-space','inherit');
});

$("#guardar_propuesta").click(function(){
    var especificaciones_propuesta = $("#especificaciones_propuesta").val();
    var descripcion_propuesta = $("#descripcion_propuesta").val();
    var fecha_entrega_propuesta = $("#fecha_entrega_propuesta").val();
    var presupuesto_propuesta = $("#presupuesto_propuesta").val();

    var id_proyecto = $("#id_proyecto").val();

    if(especificaciones_propuesta == "" || descripcion_propuesta == "" || fecha_entrega_propuesta == "" || presupuesto_propuesta == ""){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Necesitas llenar todos los campos!'
        });
    }
    else{
        var data = {
            id_usuario                  : id_usuario,
            id_proyecto                 : id_proyecto,
            especificaciones_propuesta  : especificaciones_propuesta,
            descripcion_propuesta       : descripcion_propuesta,
            fecha_entrega_propuesta     : fecha_entrega_propuesta,
            presupuesto_propuesta       : presupuesto_propuesta
        };
    
        guardar_propuesta(data);
    }
});

$("#regresar_explorar").click(function(){
    listar_proyectos();
    obtener_desempeño(id_usuario);
    $("#banner_proyectos").show(500);
    $("#banner_detalle_proyecto").slideDown(500);
    $("#banner_detalle_proyecto").hide(500);
    $('html, body').animate({scrollTop:0}, 'slow');
});

function crear_proyecto_explorar(){
    window.location = "./perfil.php";
}