var loc = window.location;
var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
var urlActual = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));

// LOGIN
    function login(correo, contrasenia){
        data = {
            correo : correo,
            contrasenia : contrasenia
        };
        $.ajax({
            url: urlActual + "routes/?route=usuario_route&controller=login",
			type: "POST",
            dataType: "JSON",
            data: {
                data : data
            },
            beforeSend: function(){
                
            },
            success: function (response){

                if(response.body=="1"){
                    Swal.fire({
                        icon: 'success',
                        title: 'Loggeado con éxito!',
                        showConfirmButton: false,
                        timer: 1500
                      });
                    setTimeout(function(){ 
                        window.location='views/explorar.php';
                    }, 1500);
                    
                }else{ 
                    if(response.body == "2" || response.body == "3"){
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'El email o la contraseña parecen ser incorrectos!'
                            //footer: '<a href>Why do I have this issue?</a>'
                        });
                    }
                    else{
                        if(response.status == "Usuario bloqueado"){
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Este usuario se encuentra bloqueado!',
                                footer: '<a href="">¿Por que tengo este problema?</a>'
                            });
                        }
                        else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Parece que llegaste a tu limite de intentos, tu cuenta será bloqueada temporalmente por motivos de seguridad!',
                                footer: '<a href="">¿Por que tengo este problema?</a>'
                            });
                        }
                    }
                    
                }
                
            }
        });

    }




