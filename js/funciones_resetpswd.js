var urlActual = "http://localhost/softwork/";
var id_usuario = "";

$("#submit_email").click(function(){
    var correo = $("#correo_reset_pswd").val();

    if(correo == ""){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Debes de ingresar tu correo!'
        });
    }
    else{
        obtener_pregunta_seguridad(correo);

    }
});

function obtener_pregunta_seguridad(correo){
    $.ajax({
        url: urlActual + "routes/?route=usuario_route&controller=get_pregunta_seguridad",
        type: "POST",
        dataType: "JSON",
        data: {
            correo : correo
        },
        beforeSend: function(){
            
        },
        success: function (response){
            if(response.data != ""){
                id_usuario = response.data.id_usuario;
                Swal.fire({
                    icon: 'success',
                    title: 'Correcto!',
                    timer: 500,
                    showConfirmButton: false,
                });
                $("#panel_reset_pswd").html("");
                $("#panel_reset_pswd").append(
                    '<div id="booking-form" >'+
                        '<div style="border-bottom: solid 2px #8c0303; width:auto;">'+
                            '<h2 style="margin: 0px !important; text-align:left; width: auto !important;">PREGUNTA DE SEGURIDAD</h2>'+
                        '</div>'+
                        '<br>'+
                        '<h4 class="header_reset">'+response.data.pregunta+'</h4>'+
                        '<div class="form-group form-input">'+
                            '<input type="text" id="respuesta_reset_pswd" required/>'+
                            '<label class="form-label">Respuesta</label>'+
                        '</div>'+
                        '<div class="group">'+
                            '<button class="button" onclick="submit_respuesta()">Aceptar</button>'+
                            '<a href="../index.php" class="vertify-booking">Iniciar Sesion!</a>'+
                        '</div>'+
                    '</div>'
                );
            }
            else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'No se encontró ningún usuario con ese correo!'
                });
            }
                
            
            
        }
    });
}

function submit_respuesta(){
    var respuesta = $("#respuesta_reset_pswd").val();
    data = {
        respuesta: respuesta,
        id_usuario : id_usuario
    }

    $.ajax({
        url: urlActual + "routes/?route=usuario_route&controller=validar_respuesta",
        type: "POST",
        dataType: "JSON",
        data: {
            data : data
        },
        beforeSend: function(){
            
        },
        success: function (response){
            if(response.data == "Correcto"){
                Swal.fire({
                    icon: 'success',
                    title: 'Correcto!',
                    showConfirmButton: false,
                    timer: 500,
                });
                $("#panel_reset_pswd").html("");
                $("#panel_reset_pswd").append(
                    '<div id="booking-form" >'+
                        '<div style="border-bottom: solid 2px #8c0303; width:auto;">'+
                            '<h2 style="margin: 0px !important; text-align:left; width: auto !important;">CONTRASEÑA NUEVA</h2>'+
                        '</div>'+
                        '<br>'+
                        '<h4 class="header_reset">Ingresa tu nueva contraseña</h4>'+
                        '<div class="form-group form-input">'+
                            '<input type="pssword" id="new_pswd" required/>'+
                            '<label class="form-label">Contraseña</label>'+
                        '</div>'+
                        '<div class="form-group form-input">'+
                            '<input type="password" id="new_pswd_confirm" required/>'+
                            '<label class="form-label">Confirmar Contraseña</label>'+
                        '</div>'+
                        '<div class="group">'+
                            '<button class="button" onclick="submit_new_pswd()">Confirmar</button>'+
                            '<a href="#../index.php" class="vertify-booking">Iniciar Sesion!</a>'+
                        '</div>'+
                    '</div>'
                );
                /*
                setTimeout(function(){ 
                    window.location='views/explorar.php';
                }, 500);*/
            }
            else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'La respuesta que ingresaste no es correcta!'
                });
            }
        }

    });
}

function submit_new_pswd(){
    pswd1 = $("#new_pswd").val();
    pswd2 = $("#new_pswd_confirm").val();


    if(pswd1.length < 8){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'La contraseña debe de tener al menos 8 dígitos!'
        });
    }else{
        if(pswd1 != pswd2){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'La contraseñas no coinciden!'
            });
        }else{
            var data = {
                id_usuario : id_usuario,
                contrasenia : pswd1
            }

            $.ajax({
                url: urlActual + "routes/?route=usuario_route&controller=cambiar_contrasenia",
                type: "POST",
                dataType: "JSON",
                data: {
                    data : data
                },
                beforeSend: function(){
                    
                },
                success: function (response){
                    id_usuario = "";
                    if(response.data == "Actualizado"){
                        Swal.fire({
                            icon: 'success',
                            title: 'Yeei!...',
                            text: 'Contraseña Actualizada!!',
                            showConfirmButton: false,
                            timer: 1200
                        });
                        setTimeout(function(){ 
                            window.location='../index.php';
                        }, 1200);
                    }
                    else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Ocurrió un error al actualizar contraseña, inténtalo mas tarde!',
                            timer: 1800
                        });
                        setTimeout(function(){ 
                            window.location='../index.php';
                        }, 1800);
                    }
                }
        
            });

        }
    }
}