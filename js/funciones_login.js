



$("#inciar_sesion").click(function(){
    correo = $("#correo_login").val();
    contrasenia = $("#contrasenia_login").val();
    
    if(correo == "" || contrasenia == ""){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Necesitas llenar ambos campos!'
            //footer: '<a href>Why do I have this issue?</a>'
        });
    }
    else{
        
        login(correo,contrasenia);
    }
});

function iniciar_sesion(){
    correo = $("#correo_login").val();
    contrasenia = $("#contrasenia_login").val();
    
    if(correo == "" || contrasenia == ""){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Necesitas llenar ambos campos!'
            //footer: '<a href>Why do I have this issue?</a>'
        });
    }
    else{
        
        login(correo,contrasenia);
    }
}


$("#contrasenia_login").keyup(function(event) {
    if (event.keyCode == 13) {
        iniciar_sesion()
    }
});
