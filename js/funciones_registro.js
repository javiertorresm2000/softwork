var usuario = {
    correo :"",
    contrasenia :"",
    pregunta_seguridad :"",
    respuesta_seguridad :"",
    nombre :"",
    apellidos :"",
    fecha_nacimiento : "",
    telefono :"",
    direccion :"",
    municipio :"",
    estado :"",
    descripcion_personal :"",
    grado_academico :"",
    carrera :"",
    experiencia_profesional :""
};
// OBETENER DATOS DE REGISTRO





$("#regresar_registro_3").click(function(){
    //$("#registro_form1").css("display", "none");
    $("#registro_form3").hide(700);
    $("#registro_form2").slideDown(700);
    $('.container').css("background-image", "url(../images/trabajo_en_casa2.jpg)"); 
    $('.container').css("background-size", "cover"); 
    $('.container').css("background-repeat", "no-repeat");
});



$('[type=date]').change( function() {
    $(this).css('white-space','normal');
  });

$("#registrarme_1").click(function(){
    correo = $("#correo").val();
    contrasenia = $("#contrasenia").val();
    confirmar_contrasenia = $("#confirmar_contrasenia").val();
    pregunta_seguridad = $("#pregunta_seguridad").val();
    respuesta_seguridad = $("#respuesta_seguridad").val();

    if(correo == "" || contrasenia == "" || confirmar_contrasenia == "" || respuesta_seguridad == ""){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Necesitas llenar los campos!'
        });
    }
    else{
        if(contrasenia.length > 7){
            if(contrasenia==confirmar_contrasenia){

                var data={
                    correo :correo,
                    contrasenia :contrasenia,
                    pregunta_seguridad :pregunta_seguridad,
                    respuesta_seguridad :respuesta_seguridad
                };

                validar_correo(data);
                
                setTimeout(function(){ 
                    if(correo_valido == "0"){   
                        usuario['correo'] = correo;
                        usuario['contrasenia'] = contrasenia;
                        usuario['pregunta_seguridad'] = pregunta_seguridad;
                        usuario['respuesta_seguridad'] = respuesta_seguridad;
    
                        $("#registro_form1").hide(700);
                        $("#registro_form2").show(700);
                        $('.container').delay(5000).css("background-image", "url(../images/trabajo_en_casa2.jpg)"); 
                        $('.container').css("background-size", "cover"); 
                        $('.container').css("background-repeat", "no-repeat"); 
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Ya existe un usuario registrado con ese correo!'
                            //footer: '<a href>Why do I have this issue?</a>'
                        });
                    }
                }, 300);
                
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Las contraseñas no coinciden!'
                    //footer: '<a href>Why do I have this issue?</a>'
                });
            }
        }else{
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'La contraseña debe de tener mas de 8 digitos!'
            });
        }
    }
});


$("#registrarme_2").click(function(){
    nombre = $("#nombre").val();
    apellidos = $("#apellidos").val();
    fecha_nacimiento = $("#fecha_nacimiento").val();
    telefono = $("#telefono").val();
    direccion = $("#direccion").val();
    municipio = $("#municipio").val();
    estado = $("#estado").val();

    if(nombre == "" || apellidos == "" || fecha_nacimiento == "" || direccion == ""){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Necesitas llenar los campos!'
        });
    }
    else{
        usuario['nombre'] = nombre;
        usuario['apellidos'] = apellidos;
        usuario['fecha_nacimiento'] = fecha_nacimiento;
        usuario['telefono'] = telefono;
        usuario['direccion'] = direccion;
        usuario['municipio'] = municipio;
        usuario['estado'] = estado;

        $("#registro_form2").slideUp(700);
        $("#registro_form3").show(700);
        setTimeout(function(){ 
            $('.container').delay(5000).css("background-image", "url(../images/trabajo_en_casa3.jpg)"); 
            $('.container').css("background-size", "cover"); 
            $('.container').css("background-repeat", "no-repeat");
        }, 700);
    }
});

$("#registrarme_3").click(function(){
    
    descripcion_personal = $("#descripcion_personal").val();
    grado_academico = $("#grado_academico").val();
    carrera = $("#carrera").val();
    experiencia = $("#experiencia").val();
    console.log(experiencia);

    if(descripcion_personal == "" || carrera == ""  || experiencia == ""){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Necesitas llenar los campos!'
        });
    }
    else{
        usuario['descripcion_personal'] = descripcion_personal;
        usuario['grado_academico'] = grado_academico;
        usuario['carrera'] = carrera;
        usuario['experiencia_profesional'] = experiencia;

        registrar_nuevo_usuario(usuario);

    }    
});


$("#regresar_registro_2").click(function(){
    $("#registro_form2").hide(700);
        $("#registro_form1").show(700);
        $('.container').css("background-image", "url(../images/trabajo_en_casa1.jpg)");
        $('.container').css("background-size", "cover"); 
        $('.container').css("background-repeat", "no-repeat"); 
});




function countChars(obj){
    document.getElementById("contador_experiencia").innerHTML = obj.value.length+' /500';
}

function countChars2(obj){
    document.getElementById("contador_descripcion").innerHTML = obj.value.length+' /150';
}