-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-06-2020 a las 19:54:58
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `softwork`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_login`
--

CREATE TABLE `detalle_login` (
  `id_detalle_login` int(11) NOT NULL,
  `fecha_login` datetime NOT NULL,
  `tiempo_login` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `usuario_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `id_estado` int(11) NOT NULL,
  `nombre_estado` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `id_municipio` int(11) NOT NULL,
  `nombre_municipio` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta_seguridad`
--

CREATE TABLE `pregunta_seguridad` (
  `id_pregunta` int(11) NOT NULL,
  `pregunta` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pregunta_seguridad`
--

INSERT INTO `pregunta_seguridad` (`id_pregunta`, `pregunta`) VALUES
(7, '¿Cuál es el auto de tus sueños?'),
(4, '¿Cuál es el nombre de tu mejor amigo?'),
(2, '¿Cuál es el nombre de tu primer mascota?'),
(1, '¿Cuál es tu comida favorita?'),
(6, '¿Cuál es tu película favorita?'),
(5, '¿Cuál es tu serie favorita?');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `propuesta`
--

CREATE TABLE `propuesta` (
  `id_propuesta` int(11) NOT NULL,
  `descripcion_general` varchar(600) NOT NULL,
  `especificacion_tecnica` varchar(400) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_entrega` date NOT NULL,
  `cotizacion` float NOT NULL,
  `usuario_propietario` int(11) NOT NULL,
  `proyecto` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `propuesta`
--

INSERT INTO `propuesta` (`id_propuesta`, `descripcion_general`, `especificacion_tecnica`, `fecha_creacion`, `fecha_entrega`, `cotizacion`, `usuario_propietario`, `proyecto`, `status`) VALUES
(1, 'Una aplicacion móvil que pueda enseñar a los alumnos a como realizar ecuaciones desde primer grado hasta las mas complejar, sera una aplicación intercativa con juegos incluidos', 'Será desarrollada en Android Studio, ocupara aproximadamante 30MB y será necesario tener Lollipop minimo como SO del celular en que el se usará la aplicación', '2020-06-14', '2020-06-30', 3200, 1, 2, 1),
(2, 'PRUEBA PRUEBA V PRUEBA PRUEBA PRUEBA PRUEBA PRUEBA PRUEBA PRUEBA PRUEBA PRUEBA ', 'PRUEBA PRUEBA PRUEBA PRUEBA V PRUEBA PRUEBA PRUEBA V PRUEBA', '2020-06-14', '2020-06-26', 3500, 1, 1, 1),
(3, '', '', '2020-06-14', '0000-00-00', 0, 1, 1, 1),
(4, '4', '4', '2020-06-14', '2020-06-16', 4000, 1, 1, 1),
(5, 'PRUEBA', 'PRUEBA', '2020-06-14', '2020-06-19', 2600, 1, 2, 1),
(6, 'Hola que tal! mi propuesta es una plaplicacion web desarrollada con Angular y en un futuro solo hacerla hibrida con Ionic, asi como tambien el back será con PHP', 'Ocuparé PHP 7, JS y Angular v9.0', '2020-06-15', '2020-07-31', 35000, 1, 3, 5),
(7, 'PPRUEBA PROPUESTA', 'PPRUEBA PROPUESTA', '2020-06-15', '2020-06-18', 5000, 1, 3, 3),
(8, 'PRUEBA 2', 'PRUEBA2', '2020-06-15', '2020-06-19', 30000, 1, 3, 3),
(9, 'Una aplicación web desarrollada en JavaScript que contemplará todos los datos necesarios o por lo menos básicos sobre el cálculo de la nomina.\n\nTambién tendrá un dashboard que permitirá agregar o quitar criterios, manipular los valores del cálculo de la nómina, etc.', 'Se hará en Javascript con un backedn de PHP, usando MVC. \n\n* Dominio Wix\n* Hosting Webempresa', '2020-06-18', '2020-07-15', 28000, 1, 4, 1),
(10, 'Aplicacion movil hibrida para iOS y para Android', 'Será desarrollada en Ionic 3 ', '2020-06-18', '2020-07-18', 2800, 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `id_proyecto` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion_general` varchar(5000) NOT NULL,
  `restricciones` varchar(5000) NOT NULL,
  `observaciones_tecnicas` varchar(5000) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_ultima_actualizacion` date DEFAULT NULL,
  `fecha_entrega_tentativa` date DEFAULT NULL,
  `fecha_entrega_aceptada` date DEFAULT NULL,
  `presupuesto_minimo` float NOT NULL,
  `presupuesto_maximo` float NOT NULL,
  `presupuesto_aceptado` float DEFAULT NULL,
  `usuario_propietario` int(11) NOT NULL,
  `usuario_aceptado` int(11) DEFAULT NULL,
  `propuesta_aceptada` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`id_proyecto`, `titulo`, `descripcion_general`, `restricciones`, `observaciones_tecnicas`, `fecha_creacion`, `fecha_ultima_actualizacion`, `fecha_entrega_tentativa`, `fecha_entrega_aceptada`, `presupuesto_minimo`, `presupuesto_maximo`, `presupuesto_aceptado`, `usuario_propietario`, `usuario_aceptado`, `propuesta_aceptada`, `status`) VALUES
(1, 'Prueba', 'La plataforma será una red social dirigida a todos aquellos basquetbolistas de la región en la cual podrán registrarse y poder comentar, publicar y ver fotografías de sus partidos, ', 'Prueba', 'Prueba', '2020-05-31', '2020-05-31', '2020-06-20', NULL, 2000, 4000, NULL, 2, NULL, NULL, 1),
(2, 'App movil escolar', 'APlicacion para ecuaciones', 'Ninguna', 'Ninguna', '2020-05-31', '2020-05-31', '2020-06-24', NULL, 2500, 3000, NULL, 1, NULL, NULL, 1),
(3, 'Red social para basquetbolistas', 'La plataforma será una red social dirigida a todos aquellos basquetbolistas de la región en la cual podrán registrarse y poder comentar, publicar y ver fotografías de sus partidos, entrenamientos, etc. Un estilo de Instagram pero dirigida específicamente a basquetbolistas.', 'Debe ser Web por el momento, la idea es después que sea una app movil, pero de primera instancia necesito que sea web\n', 'Que sea de preferencia con PHP ya que es más fácil manejarlo en cualquier server.\n\n* Disponible el uso de cualquier Framework para el desarrollo del Front-end', '2020-06-15', '2020-06-15', '2020-07-30', '2020-07-31', 30000, 35000, 35000, 1, 1, 6, 4),
(4, 'Programa para Calculo de Nomina', 'Será un programa de escritorio que ayude al calculo de nomina de un empleado operador/chofer de un camión, la aplicacion deberá tener un dashboard de administrador para que el usuario pueda ingresar los datos a través de los cuales se calculará la nomina, asi como su desempeño semanal, sueldo fijo, deducciones, comsiones, incentivos, productividad, etc.', 'No hay ninguna restriccion', 'Deberá funcionar para computadoras con Sistema Operativo iOS minimo High Sierra', '2020-06-15', '2020-06-15', '2020-06-24', NULL, 25000, 35000, NULL, 1, NULL, NULL, 1),
(5, 'Juego interactivo para aprender Ingles', 'La idea es que sea un juego para computadora de escritorio el cual cada cierto tiempo para poder seguir navegando en internet necesite adivinar o responder correctamente alguna pregunta que le haga el juego en inglés.', 'Se va a limitar únicamente a niveles de principiante el idioma ingles', 'Ninguna', '2020-06-15', '2020-06-15', '2020-06-30', NULL, 4000, 10000, NULL, 1, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_propuesta`
--

CREATE TABLE `status_propuesta` (
  `id_status` int(11) NOT NULL,
  `nombre_status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_proyecto`
--

CREATE TABLE `status_proyecto` (
  `id_status` int(11) NOT NULL,
  `nombre_status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_usuario`
--

CREATE TABLE `status_usuario` (
  `id_status` int(11) NOT NULL,
  `nombre_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `apellidos` varchar(80) NOT NULL,
  `correo` varchar(40) NOT NULL,
  `contrasenia` varchar(25) NOT NULL,
  `pregunta_seguridad` int(11) NOT NULL,
  `respuesta_seguridad` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `genero` tinyint(4) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `direccion` varchar(150) NOT NULL,
  `municipio` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `foto` varchar(150) DEFAULT NULL,
  `descripcion_personal` varchar(150) NOT NULL,
  `grado_academico` int(11) DEFAULT NULL,
  `carrera` varchar(80) DEFAULT NULL,
  `experiencia_profesional` varchar(5000) DEFAULT NULL,
  `propuestas_aceptadas` int(11) DEFAULT 0,
  `proyectos_aceptados` int(11) NOT NULL DEFAULT 0,
  `fecha_registro` date NOT NULL,
  `intentos_fallidos` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `apellidos`, `correo`, `contrasenia`, `pregunta_seguridad`, `respuesta_seguridad`, `telefono`, `genero`, `fecha_nacimiento`, `direccion`, `municipio`, `estado`, `foto`, `descripcion_personal`, `grado_academico`, `carrera`, `experiencia_profesional`, `propuestas_aceptadas`, `proyectos_aceptados`, `fecha_registro`, `intentos_fallidos`, `status`) VALUES
(1, 'Javier', 'Administrador', 'admin@gmail.com', 'administrador', 1, 'admin', '123456789', 1, '2020-06-01', 'admin', 1, 1, 'admin', 'HOLA, SOY EL ADMINISTRADOR DE ESTA PAGINA', 1, 'Adminitrador de la pagina', 'Trabajo como tester y desrrolador de la plataforma Softwork ', 0, 0, '2020-05-31', 2, 1),
(2, 'admin2', 'admin2', 'admin2@gamil.com', 'admin234', 1, 'admin234', '1234', NULL, NULL, 'admin234', 1, 1, NULL, 'admin2', 1, 'admin234', 'admin234', 0, 0, '2020-06-01', 0, 1),
(3, 'Juan', 'Perez', 'juan@gmail.com', 'juan1234', 1, 'Sushi', '4425637898', NULL, NULL, 'Av. Universidad', 1, 1, NULL, 'Diseñador de Software', 5, 'Software', '2 años', 0, 0, '2020-06-01', 0, 1),
(4, 'Javier ', 'Torres', 'javier@gmail.com', 'javier1234', 4, 'Elite', '4111186290', NULL, NULL, 'X', 1, 1, NULL, 'Front End', 4, 'Ingeniería de Software', '3 años', 0, 0, '2020-06-01', 1, 1),
(5, '1', '1', '1', '1', 1, '1', '1', NULL, '0000-00-00', '1', 1, 1, NULL, '1', 1, '1', '1', 0, 0, '2020-06-15', 0, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detalle_login`
--
ALTER TABLE `detalle_login`
  ADD PRIMARY KEY (`id_detalle_login`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`id_municipio`);

--
-- Indices de la tabla `pregunta_seguridad`
--
ALTER TABLE `pregunta_seguridad`
  ADD PRIMARY KEY (`id_pregunta`),
  ADD UNIQUE KEY `pregunta` (`pregunta`);

--
-- Indices de la tabla `propuesta`
--
ALTER TABLE `propuesta`
  ADD PRIMARY KEY (`id_propuesta`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`id_proyecto`);

--
-- Indices de la tabla `status_propuesta`
--
ALTER TABLE `status_propuesta`
  ADD PRIMARY KEY (`id_status`);

--
-- Indices de la tabla `status_proyecto`
--
ALTER TABLE `status_proyecto`
  ADD PRIMARY KEY (`id_status`);

--
-- Indices de la tabla `status_usuario`
--
ALTER TABLE `status_usuario`
  ADD PRIMARY KEY (`id_status`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalle_login`
--
ALTER TABLE `detalle_login`
  MODIFY `id_detalle_login` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `municipio`
--
ALTER TABLE `municipio`
  MODIFY `id_municipio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pregunta_seguridad`
--
ALTER TABLE `pregunta_seguridad`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `propuesta`
--
ALTER TABLE `propuesta`
  MODIFY `id_propuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `id_proyecto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
