<!DOCTYPE HTML>

<html>
	<head>
		<title>SOFTWORK</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="css/styles.css"/>
		<!-- Font Icon -->

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
		

		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		
		<script src="js/sweetalert2.all.min.js"></script>
		<script src="js/loadings.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<?php
		session_start();
		session_destroy();
	?>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.php" class="logo">SOFTWORK</a>
					<nav id="nav">
						<a href="index.php">HOME</a>
						<a href="./views/registro.php">Registrarme</a>
					</nav>
				</div>
			</header>
			<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

		<!-- Banner -->
			<section class="banner" style="background:url(https://tecreview.tec.mx/wp-content/uploads/2018/02/trabajador-independiente.jpg) no-repeat center; background-size: cover; background-repeat: no-repeat;">
				<div class="inner">
					<div class="login-wrap" >
						<div class="login-html">
							<input id="tab-1" type="radio" class="sign-in" checked>
							<label for="tab-1" class="tab">Sign In</label>
							
							<div class="login-form">
								<div class="sign-in-htm">
									<div class="form-group form-input">
                                            <input type="email"id="correo_login" value="" required />
                                            <label class="form-label">Email</label>
                                        </div>
                                        <div class="form-group form-input">
                                            <input type="password" name="phone" id="contrasenia_login" required />
                                            <label class="form-label">Password</label>
                                        </div>
									<br>
									<div class="group">
									<button class="button" id="inciar_sesion">Ingresar</button>
									</div>
									<div class="hr"></div>
									<div class="foot-lnk">
										<a href="./views/reset_pswd.php">Forgot password?</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		<!-- Footer -->
			<section id="footer">
				<div class="inner">
					
					<div class="copyright">
						&copy; SOFTWORK 2020</a>
					</div>
				</div>
			</section>

	<script src="js/funciones_login.js"></script>
    <script src="js/peticiones_login.js"></script>

	</body>
</html>